Template.edit.helpers({
  'selectedSentenceContent': function() {
    if (Narek.selectedSentenceId.get() !== null && Narek.selectedSentenceSource.get() === 'oreo') {
      highlightSelectedOreoSentence();
    }

    return Narek.selectedSentenceContent.get();
  },

  displayAutoSwap: function() {
    return Session.get('displayAutoSwap');
  },

  displayVitals: function() {
    return Session.get('displayVitals');
  }
});

Template.edit.events({
  'input #window-edit': function(e, tpl) {
    Session.set('selectedSentenceEdited', true);
  },

  'blur #window-edit': function(e, tpl) {
    console.log('blur #window-edit');

    if (Session.get('selectedSentenceEdited')) {
      if ((!e.relatedTarget || !e.relatedTarget.type || e.relatedTarget.type !== 'button') && Session.get('displayAutoSwap') === false) {
        if (tpl.$('#window-edit').text().length !== 0) {
          Narek.Sentence.changeSelectedSentence();
        }
        else {
          Narek.Sentence.changeSelectedSentence(true);
        }

        Session.set('selectedSentenceEdited', false);
      }
    }
  },

  /* Buttons */
  'click #btn-edit-original': function(e, tpl) {
    e.preventDefault();
    Narek.selectedSentenceContent.set(Narek.originalSentenceContent.get());
  },

  'click #btn-edit-andswap': function(e, tpl) {
    e.preventDefault();

    Narek.Autoswap.editSwap('and');
  },

  'click #btn-edit-commaswap': function(e, tpl) {
    e.preventDefault();

    Narek.Autoswap.editSwap('comma');
  },

  'click #btn-edit-change': function(e, tpl) {
    e.preventDefault();

    if (tpl.$('#window-edit').text().length !== 0) {
      Narek.Sentence.changeSelectedSentence();
    }
    else {
      Narek.Sentence.changeSelectedSentence(true);
    }

    if (Session.get('displayAutoSwap')) {
      Meteor.setTimeout(function() {
        Narek.Autoswap.generateSentences();
      }, 100);
    }
  },

  'click #btn-edit-prev': function(e, tpl) {
    e.preventDefault();

    Narek.Sentence.selectPreviousSentence();

    if (Session.get('displayAutoSwap')) {
      Narek.Autoswap.generateSentences();
    }
  },

  'click #btn-edit-next': function(e, tpl) {
    e.preventDefault();

    Narek.Sentence.selectNextSentence();

    if (Session.get('displayAutoSwap')) {
      Narek.Autoswap.generateSentences();
    }
  },

  'click #btn-edit-undo': function(e, tpl) {
    e.preventDefault();

    if (document.getElementsByClassName('skeleton-edit').length !== 0 || document.getElementsByClassName('answers-edit').length !== 0) {
      if (window.confirm('Do you really want to undo? Changes in Skeleton/Answer Select will be lost!')) {
        Narek.History.undo();
      }
    }
    else {
      Narek.History.undo();
    }
  },

  'click #btn-edit-redo': function(e, tpl) {
    e.preventDefault();

    Narek.History.redo();
  },

  'click #btn-edit-vitals': function(e, tpl) {
    e.preventDefault();

    Session.toggle('displayVitals');
    Session.set('displayAutoSwap', false);

    if (Session.get('displayVitals')) {
      generateVitals();
    }
  }
});
