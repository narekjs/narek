Template.autoswap.helpers({
  'hasGeneratedNewSentences': function() {
    return Session.get('hasGeneratedNewSentences');
  },

  'autoSwapSentences': function(){
    return Narek.Autoswap.AutoSwapNewSentences.find({},{
      sort: [
        ["swaps", "desc"]
      ]//,
      //limit: 50
    });
  },

  loadedFileName: function() {
    if (Session.get('loadedFileName')) {
      return Session.get('loadedFileName');
    }
    else return '';
  },

  generationTime: function() {
    return Narek.Autoswap.generationTime.get();
  },

  sentenceCount: function() {
    var count = Narek.Autoswap.sentenceCount.get() - 1;
    return count;
  }
});

Template.autoswap.events({
  'click #window-autoswap .list-group-item': function(e, tpl) {
    e.preventDefault();

    Narek.selectedSentenceContent.set(this.formatted);
  }
});
