Template.honda.helpers({});

Template.honda.events({
  'click #btn-honda-undo': function(e, tpl) {
    Narek.Honda.History.undo();
  },

  'click #btn-honda-redo': function(e, tpl) {
    Narek.Honda.History.redo();
  },

  'dragover #dropzone': function(e, tpl) {
    e.stopPropagation();
    e.preventDefault();

    e.originalEvent.dataTransfer.dropEffect = 'drop';
  },

  'drop #dropzone': function(e, tpl) {
    e.stopPropagation();
    e.preventDefault();

    var loadFiles = function() {
      var files  = e.originalEvent.dataTransfer.files;

      if (Session.equals('filesLoaded', true)) {
        Narek.Honda.History.reset();
      }

      Narek.Honda.loadFiles(files, function() {
        Narek.Honda.History.saveState('loadFiles');
      });

      if (files.length !== 0) {
        Session.set('filesLoaded', true);
      }
      else {
        Session.set('filesLoaded', false);
      }
    }

    if (Session.equals('filesLoaded', true)) {
      if (confirm('Do you really want to load these files? Currently loaded files and changes will be lost.')) {
        loadFiles();
      }
    }
    else {
      loadFiles();
    }
  },

  'click #btn-honda-save-build-file': function(e, tpl) {
    e.preventDefault();

    saveTextAsFile(tpl.find('#window-honda-build').innerText, 'HONDA-BUILD.txt');
  },

  'click #btn-honda-save-bulk': function(e, tpl) {
    e.preventDefault();

    Narek.Honda.bulkSave();
  },

  'click #btn-honda-clear-breaks': function(e, tpl) {
    e.preventDefault();

    // Clears selection breaks and saves plainText
    Narek.Text.clearSelectionBreaks();
    Narek.Honda.History.saveState('clearSelectionBreaks');
  },

  'click #btn-honda-break-sentences': function(e, tpl) {
    e.preventDefault();

    // Breaks text with newlines.
    Narek.Text.breakSelectedText();
    Narek.Honda.History.saveState('breakSelectedText');
  },

  'click #btn-honda-collapse-sentences': function(e, tpl) {
    e.preventDefault();

    // Breaks text with newlines.
    Narek.Text.collapseSelectedText();
    Narek.Honda.History.saveState('collapseSelectedText');
  },

  'click #btn-honda-remove-extra-spaces': function(e, tpl) {
    e.preventDefault();

    // Clears selection breaks and saves plainText
    Narek.Text.clearDoubleSpaces();
    Narek.Honda.History.saveState('clearDoubleSpaces');
  },

  'click #btn-honda-clean-answer-choices': function(e, tpl) {
    e.preventDefault();

    // Cleans Answer choices
    Narek.Answer.cleanChoices();
    Narek.Honda.History.saveState('cleanChoices');
  }
});

Template.hondaWindows.helpers({

  filesLoaded: function() {
    return Session.get('filesLoaded');
  },

  buildModeChecked: function() {
    if (Session.get('honda-build-mode') === true) {
      return 'checked';
    }
  },

  cutCopyModeChecked: function() {
    if (Session.get('honda-cut-copy-mode') === true) {
      return 'checked';
    }
  }
});

Template.windowBuildFile.rendered = function() {
  if (Session.get('honda-build-mode') === true) {
    Narek.Honda.deInitHondaDragAndDrop();
  }
};

Template.windowBuildFile.helpers({
  /*fullRescan: function() {
console.log('fullRescan');
    return Session.get('fullRescan');
  },*/

  buildFileSentences: function() {
    return Narek.Honda.buildFileSentences.find({}, { sort: { order: 1 } });
  },

  textDisplayBuildFile: function() {
console.log('textBuildFile');
    /*var textDisplayBuildFile = this.plainTextHondaBuild;

    var sentences = Narek.Text.readSentencesForHonda(textDisplayBuildFile)

    console.log(sentences);

    if (Session.get('fullRescan') === true) {
      Session.set('fullRescan', false);

      return '<div id="window-honda-build" class="well well-sm">' + ((textDisplayBuildFile) ? textDisplayBuildFile : '') + '</div>';
    }
    else {
      return ((textDisplayBuildFile) ? textDisplayBuildFile : '');
    }

    return textDisplayBuildFile;
    */
  }
});

Template.windowBuildFile.events({
  'input #window-honda-build': function(e, tpl) {
    console.log('build-input');
    // Narek.Honda.buildFileChanged.set(true);
  },

  'blur #window-honda-build': function(e, tpl) {
    console.log('build-blur');
    // if (Narek.Honda.buildFileChanged.get() === true) {
      // Narek.Text.rescanSentences();
      // Narek.Honda.History.saveState('blurHondaBuild');
    // }
  }
});

Template.hondaWindows.events({
  'click #checkbox-build-mode': function(e, tpl) {
    e.preventDefault();

    Narek.Honda.toggleBuildMode(e.target.checked);
  },

  'click #checkbox-cut-copy-mode': function(e, tpl) {
    e.preventDefault();

    Narek.Honda.toggleCutCopyMode(e.target.checked);
  }
});

Template.windowHondaSource.rendered = function() {
  if (Session.get('filesLoaded') === true) {
    this.$('#loaded-files').sortable({
      handle: '.sortable-handle',
      axis: 'x',
      stop: function(e, ui) {
        Narek.Honda.updateSortableOrder(ui, Narek.Honda.loadedFilesCollection);
      }
    });
  }
};

Template.windowHondaSource.helpers({
  loadedFiles: function() {
    return Narek.Honda.loadedFilesCollection.find({}, { sort: { order: 1 } });
  },

  autoBreakDisabled: function() {
    return Session.get(this.windowId + 'autobreak-disabled');
  },

  autoBreakChecked: function() {
    if (Session.get(this.windowId + '-checked') === true) {
      return 'checked';
    }
    else {
      return '';
    }
  },

  getSentencesCollection: function() {
    var collectionName = this.content.sortableSentencesCollection;

    if (Narek.sentencesCollections[collectionName]) {
      var collectionSentences = Narek.sentencesCollections[collectionName].find({}, { sort: { order: 1 } });
    }

    if (collectionSentences) {
      return collectionSentences;
    }
  },

  editingName: function() {
    if (Session.get('editingName') === this.windowId) {
      return true;
    }
    else {
      return false;
    }
  }
});

Template.windowHondaSource.events({
  'click #checkbox-auto-break': function(e, tpl) {
    e.preventDefault();

    if (!Session.equals(this.windowId + 'autobreak-disabled', true)) {
      if (Session.equals('hondaContentChanged', false)) {
        Session.set(this.windowId + '-checked', e.target.checked);
      }
      else if (!Session.equals('hondaContentChanged', this.windowId)) {
        Session.set(this.windowId + '-checked', e.target.checked);
      }
    }
  },

  'dblclick .name': function(e, tpl) {
    e.preventDefault();

    Session.set('editingName', this.windowId);
  },

  'click #save-file-name': function(e, tpl) {
    e.preventDefault();

    var newName = tpl.find('.input-file-name').value;

    Narek.Honda.updateFileName(newName, this._id);
    Session.set('editingName', false);
  },

  'click .icon.close': function(e, tpl) {
    e.preventDefault();

    var result = confirm('Do you really want to close this window?');

    if (result == true) {
      Narek.Honda.loadedFilesCollection.remove(this._id);

      Narek.Honda.History.loadedFilesWindowIdsAndContent.splice(Narek.Honda.History.loadedFilesWindowIdsAndContent.indexOf(this.windowId), 1);

      Narek.Honda.updateLoadedFilesOrder();
      Narek.Honda.History.saveState('closeSourceWindow');
    }
  },

  'click .sentence': function(e, tpl) {
    e.preventDefault();

    if (!!Narek.showMain.get()) {
      Narek.plainTextKitkat.set(this.plainText);
      Narek.loadedFileIndex = this.order;
      Session.set('loadedFileName', this.name);
      $('.btn-honda-autoswap').removeClass('btn-primary').addClass('btn-success');
      $(e.target).parent().parent()
        .find('.btn-honda-autoswap').addClass('btn-primary').removeClass('btn-success');

      Meteor.setTimeout(function() {
        $('#window-kitkat span[num=' + e.target.getAttribute("num") + ']').trigger('click');
      });
    }

    if (Session.get('honda-build-mode') === true) {
      Narek.Honda.addSentenceToBuildFile(e.target.innerText);
      Narek.Honda.History.saveState('clickSentenceHonda');
    }
  },

  'click .btn-honda-autoswap': function(e, tpl) {
    e.preventDefault();

    if (!$(e.target).hasClass('btn-primary')) {
      Narek.showMain.set(true);
    } else {
      // Update Honda window
      Narek.Honda.upsertHondaSourcePlainText(Narek.plainTextKitkat.get(), this._id, this);
      $('#btn-kitkat-autoswap').trigger('click'); // Close autoswap
      Narek.showMain.set(false);
    }

    $('.btn-honda-autoswap').removeClass('btn-primary');
    $('.btn-honda-autoswap').addClass('btn-success');

    if (!!Narek.showMain.get()) {
      Narek.plainTextKitkat.set(this.plainText);

      $(e.target).removeClass('btn-success');
      $(e.target).addClass('btn-primary');

      Narek.loadedFileIndex = this.order;
      Session.set('loadedFileName', this.name);

      Meteor.setTimeout(function(){
        $('#btn-kitkat-autoswap').trigger('click');
      }, 100);
    }
  },

  'click .btn-honda-scramble': function(e, tpl) {
    e.preventDefault();

    Narek.Answer.scrambleAnswers(this);
    Narek.Honda.History.saveState('scrambleAnswers');
  },

  'click .btn-honda-gender-swap': function(e, tpl) {
    e.preventDefault();

    var result = Narek.Text.swapGenders(this.plainText, 'honda');

    Narek.Honda.upsertHondaSourcePlainText(result, this._id, this);
    Narek.Honda.History.saveState('hondaGenderSwap');
  },

  'click .btn-honda-save-source-file': function(e, tpl) {
    e.preventDefault();

    saveTextAsFile(e.target.parentElement.children[1].innerText, this.name + '-HONDA.txt');
  }
});

Template.windowHondaSourceSentences.helpers({
  autoBreakChecked: function() {
    if (Session.get(this.windowId + '-checked') === true) {
      return 'checked';
    }
  },
});

Template.windowHondaSourceBrokenSentences.rendered = function() {
  if (Session.equals('honda-build-mode', true)) {
    Narek.Honda.initHondaDragAndDrop();
  }
};

Template.windowHondaSourceBrokenSentences.helpers({
  isEditable: function() {
    if (Session.equals('honda-build-mode', true)) {
      return false;
    }
    else {
      return true;
    }
  }
});

Template.windowHondaSourceBrokenSentences.events({
  'input .source-content': function(e, tpl) {
    Session.set('hondaContentChanged', this.windowId);
  },

  'blur .source-content': function(e, tpl) {
    if (Session.equals('hondaContentChanged', this.windowId)) {
      var contentText = e.currentTarget.innerText;

      Session.set(this.windowId + 'autobreak-disabled', true);

      Narek.Honda.upsertHondaSourcePlainText(contentText, this._id, this);
      Narek.Honda.History.saveState('hondaSourceBlur');
      e.currentTarget.innerText = '';
      Session.set('hondaContentChanged', false);
    }
  }
});

Template.windowHondaSourcePlainSentences.rendered = function() {
  if (Session.get('honda-build-mode') === true) {
    Narek.Honda.initHondaDragAndDrop();
  }
};

Template.windowHondaSourcePlainSentences.events({
  'input .source-content': function(e, tpl) {
    Session.set('hondaContentChanged', this.windowId);
  },

  'blur .source-content': function(e, tpl) {
    if (Session.equals('hondaContentChanged', this.windowId)) {
      var contentText = e.target.innerText;

      Narek.Honda.upsertHondaSourcePlainText(contentText, this._id, this);
      Narek.Honda.History.saveState('hondaSourceBlur');
      e.target.innerText = '';
      Session.set('hondaContentChanged', false);
    }
  }
});

Template.windowHondaSourcePlainSentences.helpers({
  helperPlainSentences: function(plainSentences) {
    if (Session.get('honda-build-mode') === true) {
      return plainSentences.join('<br>');
    }
    else {
      return plainSentences.join('<br>');
    }
  },

  buildMode: function() {
    if (Session.get('honda-build-mode') === true) {
      return false;
    }
    else {
      return true;
    }
  }
});
