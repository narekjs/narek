Template.windowKitkat.helpers({
  fullRescan: function () {
    return Session.get('fullRescan');
  },

  textDisplayKitkat: function() {
    var textDisplayKitkat = this.plainTextKitkat;

    if (textDisplayKitkat) {
      var lines = readLines(textDisplayKitkat, 'kitkat');
      textDisplayKitkat = lines.join('<br>');

      // Set to be available in duplicate check.
      Session.set('textDisplayKitkat', textDisplayKitkat);

      if (Session.get('duplicateCheck')) {
        textDisplayKitkat = Narek.Text.highlightDuplicates(textDisplayKitkat, 'kitkat');
      }

      if (Session.get('differenceCheck')) {
        textDisplayKitkat = Narek.Kitkat.highlightDifferences(textDisplayKitkat);
      }

      if (!Session.get('duplicateCheck') && !Session.get('differenceCheck') && Session.get('genderHightlighted')) {
        textDisplayKitkat = Narek.Text.highlightGender(textDisplayKitkat);
      }
    }

    if (Narek.selectedSentenceId.get() !== null && Narek.selectedSentenceSource.get() === 'kitkat') {
      highlightSelectedKitkatSentence();
    }

    if (Session.get('fullRescan') === true) {
      Session.set('fullRescan', false);
      return '<div id="window-kitkat" data-name="textKitkat" class="form-control" contenteditable="true">' + ((textDisplayKitkat) ? textDisplayKitkat : '') + '</div>';
    }
    else {
      return ((textDisplayKitkat) ? textDisplayKitkat : '');
    }
  }
});

Template.kitkat.helpers({
  sentenceMode: function() {
    return Session.get('sentenceMode');
  },

  genderMale: function() {
    return Session.get('genderMale');
  },

  genderFemale: function() {
    return Session.get('genderFemale');
  },

  loadedFileName2: function() {
    if (Session.get('loadedFileName2')) {
      return '<span style="font-size: 12px">' + Session.get('loadedFileName2') + '</span>';
    }
    else return '';
  },

  loadedFolder: function() {
    if (Session.get('loadedFolder')) {
      return true;
    }
    else {
      return false;
    }
  },

  displayAutoSwap: function() {
    return Session.get('displayAutoSwap');
  },

  // From answer.js
  dragAndDropActive: function() {
    return Session.get('dragAndDropActive');
  }
});

Template.kitkat.events({
  'click #btn-sentence-mode': function(e, tpl) {
    e.preventDefault();

    Narek.Helper.toggleSentenceMode();
  },

  'click #btn-oreo-drag-and-drop': function(e, tpl) {
    e.preventDefault();

    Narek.Helper.toggleDragAndDrop();
  },

  'mouseenter #window-kitkat span.sentence': function(e, tpl) {
    var num = $(e.target).attr('num');

    $('#layout-top span[num=' + num + ']').addClass('sentence-hover');
  },

  'mouseleave #window-kitkat span.sentence': function(e, tpl) {
    var num = $(e.target).attr('num');

    $('#layout-top span[num=' + num + ']').removeClass('sentence-hover');
  },

  'click #window-kitkat span.sentence': function(e, tpl) {
    e.preventDefault();

    if (!Session.get('displayOriginal')) {
      var target = $(e.currentTarget);

      if (Session.equals('sentenceMode', true)) {
        Narek.selectedSentenceId.set(target.attr('num'));
        Narek.selectedSentenceContent.set(target[0].textContent);
        Narek.selectedSentenceSource.set('kitkat');
        Narek.originalSentenceContent.set(target[0].textContent);

        selectText('window-edit');
      }

      if (Session.get('displayAutoSwap')) {
        progressJs().start();
        Narek.Autoswap.generateSentences();
      }

      if (Session.get('displayVitals')) {
        generateVitals();
      }
    }
  },

  'keydown .answers-edit, keydown .skeleton-edit': function(e, tpl) {
    if (e.keyCode == '8') {
      if (e.target.textContent.length === 0) {
        e.preventDefault();
        e.stopPropagation();

        return false;
      }
    }
  },

  'dragover #window-kitkat': function(e, tpl) {
    e.stopPropagation();
    e.preventDefault();

    e.originalEvent.dataTransfer.dropEffect = 'copy';
  },

  'drop #window-kitkat': function(e, tpl) {
    e.stopPropagation();
    e.preventDefault();
    var file  = e.originalEvent.dataTransfer.files[0];

    Session.set('oreoLocked', false);
    Session.set('loadedFileName2', file.name.replace(/\.[a-z0-9]+$/i, ''));
    Narek.File.readFile(file, Narek.File.loadSelectedFileKitkat);
  },

  'paste #window-kitkat': function(e, tpl) {
console.log('paste');
console.log('e', e);
    if (Narek.plainTextKitkat.get() === '') {
console.log('if');
      e.preventDefault();
      e.stopPropagation();

      Narek.plainTextKitkat.set(e.originalEvent.clipboardData.getData('text'));

      Meteor.setTimeout(function() {
        // tpl.$('#window-kitkat').focus();
      }, 100);
    }
    else if ($(e.target).hasClass('skeleton-edit') || $(e.target).hasClass('answers-edit')) {
console.log('else-if');
      e.preventDefault();
      e.stopPropagation();
      insertTextAtCursor(e.originalEvent.clipboardData.getData('text'));
      // tpl.$(e.target).append(e.originalEvent.clipboardData.getData('text'))
    }
    else {
console.log('else');
      e.preventDefault();
      e.stopPropagation();
      insertTextAtCursor(e.originalEvent.clipboardData.getData('text'));
    }
  },

  'input #window-kitkat': function(e, tpl) {
    var textKitkat = document.getElementById('window-kitkat').innerText;

    if (!Session.equals('oreoLocked', true) && (Narek.Kitkat.answerAdded !== true) && Narek.Kitkat.skeletonAdded !== true) {
      Narek.plainTextOreo.set(textKitkat);
    }

    Session.set('kitkatChanged', true);

    if (Session.equals('dragAndDropActive', true)) {
      Session.set('dragAndDropActive', false);
    }
  },

  'click #window-kitkat': function(e, tpl) {
    e.preventDefault();
    e.stopPropagation();

    if (e.target.classList.contains('sentence') === false) {
      $('.sentence-highlighted').removeClass('sentence-highlighted');
    }
  },

  'focus': function(e, tpl) {
    // console.log(Template.currentData());
    e.currentTarget.setAttribute('data-content', Template.currentData());

    if (e.target.innerHTML === '') {}
  },

  'blur #window-kitkat': function(e, tpl) {
    // if (!e.relatedTarget || !e.relatedTarget.type || e.relatedTarget.type !== 'button') {
      // if (document.getSelection().toString().length === 0)
      if (Narek.Kitkat.answerAdded !== true && $('#window-kitkat .correct-answer').length !== 0 && Session.get('duplicateCheck') === false) {
        Narek.Answer.updateCorrectAnswer();
      }

      if (Session.equals('kitkatChanged', true)) {

        if ((Narek.Kitkat.answerAdded !== true) && Narek.Kitkat.skeletonAdded !== true) {
          // Narek.Text.rescanSentences();
          var contentText = e.currentTarget.innerText;

          if (contentText !== Narek.plainTextKitkat.get()) {
            e.currentTarget.innerText = '';
          }

          Narek.plainTextKitkat.set(contentText);
          Session.set('kitkatChanged', false);
          Narek.History.saveState('kitkatChanged');
        }
      }
    // }
  },

  'click #btn-kitkat-save': function(e, tpl) {
    e.preventDefault();

    saveTextAsFile($('#window-kitkat')[0].innerText, Session.get('loadedFileName') + '-KITKAT.txt');
  },

  'click #btn-kitkat-delete-answers': function(e, tpl) {
    e.preventDefault();

    Narek.Helper.deleteAnswers();

    Narek.Kitkat.answerAdded = false;
    Narek.plainTextKitkat.set(document.getElementById('window-kitkat').innerText);
    Narek.History.saveState('deleteAnswers');
  },

  'click #btn-kitkat-delete-skeleton': function(e, tpl) {
    e.preventDefault();

    Narek.Helper.deleteSkeleton();

    Narek.Kitkat.skeletonAdded = false;
    Narek.plainTextKitkat.set(document.getElementById('window-kitkat').innerText);
    Narek.History.saveState('deleteSkeleton');

    Session.set('sentenceMode', true);
  },

  'click #btn-kitkat-clean-answer-choices': function(e, tpl) {
    e.preventDefault();

    Narek.Answer.cleanChoices();
    Narek.History.saveState('cleanChoices');
  },

  'click #btn-kitkat-autoswap': function(e, tpl) {
    e.preventDefault();
    Narek.Helper.toggleAutoSwap();
  },

  'click #btn-kitkat-selection-autoswap': function(e, tpl) {
    e.preventDefault();
    Narek.Autoswap.selectionAutoSwap();
  },

  'click #btn-kitkat-bulk-autoswap': function(e, tpl) {
    e.preventDefault();
    Narek.Autoswap.bulkAutoSwap();
  },

  'click #btn-kitkat-bulk-fileswap': function(e, tpl) {
    e.preventDefault();
    Narek.Autoswap.bulkFileSwap();
  },

  'click #btn-kitkat-remove-extra-spaces': function(e, tpl) {
    e.preventDefault();

    // Clears selection breaks and saves plainText
    Narek.Text.clearDoubleSpaces();
    Narek.History.saveState('clearDoubleSpaces');
  },

  'click #btn-kitkat-scramble-answers': function(e, tpl) {
    e.preventDefault();

    if (Narek.plainTextKitkat.get() !== '') {
      Narek.Answer.scrambleAnswers();
      Narek.History.saveState('scrambleAnswers');
    }
  }
});
