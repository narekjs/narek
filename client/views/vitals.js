VitalsSentences = new Mongo.Collection(null);

randomizeNumber = function(number) {
  var min, max, newNumber;
  do {
    if (number % 1 !== 0) {
      min = number - 0.2;
      max = number + 0.2;
      if (min < 0) {
        min = 0;
      }
      newNumber = (Math.random() * (max - min + 1)) + min;
    }
    else {
      min = number - 2;
      max = number + 2;
      if (min < 0) {
        min = 0;
      }
      newNumber = Math.floor(Math.random() * (max - min + 1)) + min;
    }
  } while (number == newNumber);
  return newNumber;
}

generateVitals = function() {

  Session.set('hasGeneratedVitals', false);

  var temperaturePrefixes = ["Temperature of", "T", "Temp", "temperature", "temeprature"].join('|');
  var pulsePrefixes = ["Pulse", "P", "HR", "heart rate", "heartrate"].join('|');
  var bloodPressurePrefixes = ["Blood pressure", "BP", "blood-pressure"].join('|');
  var bloodPressureSuffixes = ["mmHg", "mm Hg"].join('|');
  var respirationsSuffixes = ["Respirations", "RR", "respiratory-rate", "respiratory rate"].join('|');

  var sentence = Narek.selectedSentenceContent.get();

  var regex = new RegExp('\\b(?:' + pulsePrefixes + ').?(?:is|of)?\\s(\\d{1,3})', 'gi');
  var tempResult = regex.exec(sentence);
  var pulse;
  if (tempResult && tempResult.length > 1) {
    pulse = randomizeNumber(Number(tempResult[1])); // [1] contains the number
  }

  var regex = new RegExp('\\b(?:' + temperaturePrefixes + ').?(?:is|of)?\\s(\\d{1,3}[.]?\\d?).?(C|F)', 'gi');
  var tempResult = regex.exec(sentence);
  var scale, temperature;
  if (tempResult && tempResult.length > 2) {
    scale = tempResult[2]; // 'C' or 'F'
    temperature = randomizeNumber(Number(tempResult[1]));
  }

  var regex = new RegExp('\\b(?:' + bloodPressurePrefixes + ').?(?:is|of)?\\s(\\d{1,3})\/(\\d{1,3})', 'gi');
  var tempResult = regex.exec(sentence);
  var bloodPressure, bloodPressureFirst, bloodPressureSecond;
  if (tempResult && tempResult.length > 2) {
    bloodPressureFirst = Number(tempResult[1]);
    bloodPressureSecond = Number(tempResult[2]);
    bloodPressure = randomizeNumber(bloodPressureFirst) + '/' + randomizeNumber(bloodPressureSecond);
  }

  var regex = new RegExp('\\b(?:' + respirationsSuffixes + ').?(?:is|of)?\\s(\\d{1,3})', 'gi');
  var tempResult = regex.exec(sentence);
  var respirations;
  if (tempResult && tempResult.length > 1) {
    respirations = randomizeNumber(Number(tempResult[1]));
  }

  Meteor.setTimeout(function(){
    if (scale === 'C') {
      $('#vitals-input-tc').val(temperature);
      Session.set('vitalsTempC', temperature);
      $('#vitals-input-tc').trigger('change');
    }
    else if (scale === 'F') {
      $('#vitals-input-tf').val(temperature);
      Session.set('vitalsTempF', temperature);
      $('#vitals-input-tf').trigger('change');
    }

    $('#vitals-input-p').val(pulse);
    Session.set('vitalsPulse', pulse);

    $('#vitals-input-bp').val(bloodPressure);
    Session.set('vitalsBloodPressure', bloodPressure);

    $('#vitals-input-rr').val(respirations);
    Session.set('vitalsRespiratoryRate', respirations);

    generateVitalsSentences();
  }, 100);

};

generateVitalsSentences = function() {
  VitalsSentences.remove({}); // Reset the collection

  var gender = $('#gender-radio-buttons-group .active input').val();
  var genderPronoun = 'His';
  if (gender === 'female') {
    genderPronoun = 'Her';
  }

  var sentences = [
    "Vitals are ",
    "Vitals show ",
    "On exam vitals show ",
    "On exam vitals are ",
    "On physical exam vitals are ",
    "On physical exam vitals show ",
    "Vitals signs of the patient are as follows ",
    "The patient's vitals are ",
    "The following are the vitals for this patient ",
    genderPronoun + " vitals show "
  ];

  var pulse = Session.get('vitalsPulse');
  var bloodPressure = Session.get('vitalsBloodPressure');
  var respiratoryRate = Session.get('vitalsRespiratoryRate');
  var tempC = Session.get('vitalsTempC');
  var tempF = Session.get('vitalsTempF');
  var o2 = Session.get('vitalsO2');

  if (pulse) {
    sentences = _.map(sentences, function(sentence) {
      return sentence + 'P: ' + pulse + '/min, ';
    }).concat(_.map(sentences, function(sentence) {
      return sentence + 'HR: ' + pulse + '/min, ';
    }));
    Session.set('hasGeneratedVitals', true);
  }

  if (bloodPressure) {
    sentences = _.map(sentences, function(sentence) {
      return sentence + 'BP: ' + bloodPressure + ' mmHg, ';
    });
    Session.set('hasGeneratedVitals', true);
  }

  if (respiratoryRate) {
    sentences = _.map(sentences, function(sentence) {
      return sentence + 'RR: ' + respiratoryRate + '/min, ';
    });
    Session.set('hasGeneratedVitals', true);
  }

  if (tempC) {
    sentences = _.map(sentences, function(sentence) {
      return sentence + 'T: ' + tempC + '°C (' + tempF + '°F)';
    });
    Session.set('hasGeneratedVitals', true);
  }

  if (o2) {
    sentences = _.map(sentences, function(sentence) {
      return sentence + ', O2 SAT: ' + o2 + '%';
    }).concat(_.map(sentences, function(sentence) {
      return sentence + ', Pulse Ox: ' + o2 + '%';
    }));
    Session.set('hasGeneratedVitals', true);
  }
  else {
    sentences = _.map(sentences, function(sentence) {
      return sentence + '.';
    });
  }

  if (Session.get('hasGeneratedVitals')) {
    var sentenceCount = sentences.length;
    for (var i = 0; i < sentenceCount; i++) {
      VitalsSentences.insert({txt: sentences[i]});
    }
  }
};

Template.vitals.helpers({
  'hasGeneratedVitals': function() {
    return Session.get('hasGeneratedVitals');
  },

  'vitalsSentences': function(){
    return VitalsSentences.find({},{
      sort: [
        ["txt", "asc"]
      ]
    });
  },

  loadedFileName: function() {
    if (Session.get('loadedFileName')) {
      return '<span style="font-size: 12px">#' + Narek.loadedFileIndex + ': ' + Session.get('loadedFileName') + '</span>';
    }
    else return '';
  }
});

Template.vitals.events({
  'click .list-group-item': function(e, tpl) {
    e.preventDefault();
    $('#window-edit').text(this.txt);
  },

  'input #vitals-input-p': function(e, tpl) {
    e.preventDefault();
    var value = e.target.value;
    Session.set('vitalsPulse', value);

    generateVitalsSentences();
  },

  'input #vitals-input-bp': function(e, tpl) {
    e.preventDefault();
    var value = e.target.value;
    Session.set('vitalsBloodPressure', value);

    generateVitalsSentences();
  },

  'input #vitals-input-rr': function(e, tpl) {
    e.preventDefault();
    var value = e.target.value;
    Session.set('vitalsRespiratoryRate', value);

    generateVitalsSentences();
  },

  'change #vitals-input-tc': function(e, tpl) {
    e.preventDefault();
    var value = Number(e.target.value).toFixed(1);
    var fahrenheitValue = (value * 9 / 5 + 32).toFixed(1);
    $('#vitals-input-tf').val(fahrenheitValue);
    $('#vitals-input-tc').val(value);
    Session.set('vitalsTempC', value);
    Session.set('vitalsTempF', fahrenheitValue);

    generateVitalsSentences();
  },

  'change #vitals-input-tf': function(e, tpl) {
    e.preventDefault();
    var value = Number(e.target.value).toFixed(1);
    var celsiusValue = ((value - 32) * 5 / 9).toFixed(1);
    $('#vitals-input-tc').val(celsiusValue);
    $('#vitals-input-tf').val(value);
    Session.set('vitalsTempF', value);
    Session.set('vitalsTempC', celsiusValue);

    generateVitalsSentences();
  },

  'input #vitals-input-o2': function(e, tpl) {
    e.preventDefault();
    var value = e.target.value;
    Session.set('vitalsO2', value);

    generateVitalsSentences();
  }
});
