window.onbeforeunload = function() {
  return "Dude, are you sure you want to leave? Think of the kittens!";
}

Template.body.events({
  'keyup': function(e, tpl) {
    e.preventDefault();
    e.stopPropagation();
    event.returnValue = false;
    // CTRL is pressed
    if (e.ctrlKey) {
      // ALT is pressed
      if (e.altKey) {
        if (e.shiftKey) {
          switch (e.keyCode) {
            // CTRL UP = UNDO
            case 38:
              if (Router.current().route.getName() === 'honda') {
                Narek.Honda.History.undo();
              }
              else {
                if (document.getElementsByClassName('skeleton-edit').length !== 0 || document.getElementsByClassName('answers-edit').length !== 0) {
                  if (window.confirm('Do you really want to undo? Changes in Skeleton/Answer Select will be lost!')) {
                    Narek.History.undo();
                  }
                }
                else {
                  Narek.History.undo();
                }
              }
            break;
            // CTRL DOWN = REDO
            case 40:
              if (Router.current().route.getName() === 'honda') {
                Narek.Honda.History.redo();
              }
              else {
                Narek.History.redo();
              }
            break;
          }
        }
        else {
          switch (e.keyCode) {
            // CTRL + ENTER = CHANGE
            case 13:
              $('#btn-edit-change').click();
            break;

            // CTRL LEFT = PREVIOUS
            case 37:
              Narek.Sentence.selectPreviousSentence();
            break;
            // CTRL RIGHT = NEXT
            case 39:
              Narek.Sentence.selectNextSentence()
            break;

            // CTRL + ALT + G = DRAG & DROP
            case 71:
              if (Router.current().route.getName() == 'honda') {
                Narek.Honda.toggleBuildMode(!Session.get('honda-build-mode'));
              }
              else {
                Narek.Helper.toggleDragAndDrop();
              }
            break;
            // CTRL + ALT + S = SENTENCE MODE
            case 83:
              Narek.Helper.toggleSentenceMode();
            break;

            // CTRL + ALT + A = AUTOSWAP
            case 65:
              Narek.Helper.toggleAutoSwap();
            break;
            // CTRL + ALT + R = RESCAN
            case 82:
              Narek.Text.rescanSentences();
            break;
          }
        }
      }
    }
  }
});

Template.main.onRendered(function() {
  $('html, body').scrollTop($(document).height());
});
