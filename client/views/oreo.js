Template.windowOreo.helpers({
  fullRescan: function () {
    return Session.get('fullRescan');
  },

  textDisplayOreo: function() {
    if (Session.get('displayOriginal')) {
      var textDisplayOreo = Session.get('originalText');
    }
    else {
      var textDisplayOreo = this.plainTextOreo;
    }

    if (textDisplayOreo) {
      var lines = readLines(textDisplayOreo, 'oreo');
      textDisplayOreo = lines.join('<br>');

      // Set to be available in duplicate check.
      Session.set('textDisplayOreo', textDisplayOreo);

      if (Session.get('duplicateCheck')) {
        textDisplayOreo = Narek.Text.highlightDuplicates(textDisplayOreo, 'oreo');
      }

      if (!Session.get('duplicateCheck') && !Session.get('differenceCheck') && Session.get('genderHightlighted')) {
        textDisplayOreo = Narek.Text.highlightGender(textDisplayOreo);
      }
    }

    if (Narek.selectedSentenceId.get() !== null && Narek.selectedSentenceSource.get() === 'oreo') {
      highlightSelectedOreoSentence();
    }

    if (Session.equals('displayOriginal', true)) {
      return textDisplayOreo;
    }
    else {
      if (Session.get('fullRescan') === true) {
        Session.set('fullRescan', false);
        return '<div id="window-oreo" data-name="textOreo" class="form-control" contenteditable="true">' + ((textDisplayOreo) ? textDisplayOreo : '') + '</div>';
      }
      else {
        return ((textDisplayOreo) ? textDisplayOreo : '');
      }
    }
  },
});

Template.oreo.helpers({
  oreoLocked: function() {
    return Session.get('oreoLocked');
  },
  genderHightlighted: function() {
    // TODO: change variable name.
    return Session.get('genderHightlighted');
  },
  sentenceMode: function() {
    return Session.get('sentenceMode');
  },
  displayAutoSwap: function() {
    return Session.get('displayAutoSwap');
  },
  displayOriginal: function() {
    return Session.get('displayOriginal');
  },
  displayVitals: function() {
    return Session.get('displayVitals');
  },
  loadedFileName: function() {
    if (Session.get('loadedFileName')) {
      return '<span style="font-size: 12px">' + Session.get('loadedFileName') + '</span>';
    }
    else return '';
  }
});

Template.oreo.events({
  /* File loading */
  'click #btn-oreo-lock': function(e, tpl) {
    e.preventDefault();

    Session.set('oreoLocked', true);
  },

  'change #input-file': function(e, tpl) {
    e.preventDefault();
    var file  = e.target.files[0];
    Narek.File.readFile(file, Narek.File.loadSelectedFile);
  },

  // TODO: Session variable for prev/next file number in array
  'change #input-folder': function(e, tpl) {
    e.preventDefault();
    Narek.loadedFiles = e.target.files;
    Narek.createFilePairs();
    Narek.createOreoFileList(); // For bulk file autoswapping
    Narek.loadFilePair('first');
  },

  'click #btn-prev-file': function(e, tpl) {
    e.preventDefault();
    Narek.loadFilePair('prev');
  },

  'click #btn-next-file': function(e, tpl) {
    e.preventDefault();
    Narek.loadFilePair('next');
  },

  'dragover #window-oreo': function(e, tpl) {
    e.stopPropagation();
    e.preventDefault();

    e.originalEvent.dataTransfer.dropEffect = 'copy';
  },

  'drop #window-oreo': function(e, tpl) {
    e.stopPropagation();
    e.preventDefault();
    var file  = e.originalEvent.dataTransfer.files[0];

    Session.set('loadedFileName', file.name.replace(/\.[a-z0-9]+$/i, ''));
    Narek.File.readFile(file, Narek.File.loadSelectedFile);
  },

  /* Sentence highlight */
  'mouseenter #window-oreo span.sentence': function(e, tpl) {
    $('#layout-top span[num=' + $(e.target).attr('num') + ']').addClass('sentence-hover');
  },

  'mouseleave #window-oreo span.sentence': function(e, tpl) {
    $('#layout-top span[num=' + $(e.target).attr('num') + ']').removeClass('sentence-hover');
  },

  'click #window-oreo span.sentence': function(e, tpl) {
    e.preventDefault();

    if (!Session.get('displayOriginal')) {
      var target = $(e.currentTarget);

      $('#layout-top span').removeClass('sentence-highlighted');
      // $('#layout-top span[num=' + target.attr('num') + ']').addClass('sentence-highlighted');

      if (Session.equals('sentenceMode', true)) {
        Narek.selectedSentenceId.set(target.attr('num'));
        Narek.selectedSentenceContent.set(target[0].textContent);
        Narek.selectedSentenceSource.set('oreo');
        Narek.originalSentenceContent.set(target[0].textContent);

        selectText('window-edit');
      }
    }
  },

  'paste #window-oreo': function(e, tpl) {
    if (Narek.plainTextOreo.get() === '') {
      e.preventDefault();
      e.stopPropagation();

      Narek.plainTextOreo.set(e.originalEvent.clipboardData.getData('text'));
      Narek.plainTextKitkat.set(e.originalEvent.clipboardData.getData('text'));
    }
  },

  'input #window-oreo': function(e, tpl) {
    var textOreo = document.getElementById('window-oreo').innerText;

    if (!Session.equals('oreoLocked', true)) {
      Narek.plainTextKitkat.set(textOreo);
    }

    Session.set('oreoChanged', true);

    if (Session.get('dragAndDropActive') === true) {
      Session.set('dragAndDropActive', false);
    }
  },

  'click #window-oreo': function(e, tpl) {
    e.preventDefault();
    e.stopPropagation();

    if (e.target.classList.contains('sentence') === false) {
      $('.sentence-highlighted').removeClass('sentence-highlighted')
    }
  },

  'blur #window-oreo': function(e, tpl) {
    // if (!e.relatedTarget || !e.relatedTarget.type || e.relatedTarget.type !== 'button') {
      var textOreo = document.getElementById('window-oreo').innerText;

      if (Session.equals('oreoChanged', true)) {
        var contentText = e.target.innerText,
          plainTextOreo = Narek.plainTextOreo.get();

        if (Narek.plainTextOreo.get() === '') {
          Narek.Text.rescanSentences();
        }
        else {
          if (contentText !== plainTextOreo) {
            e.target.innerText = '';
          }

          Narek.plainTextOreo.set(contentText);
        }
        Session.set('oreoChanged', false);
        Narek.History.saveState('oreoChanged');
      }
    // }
  },

  /* Buttons */
  'click #btn-oreo-gender-highlight': function(e, tpl) {
    e.preventDefault();

    Session.set('genderHightlighted', !Session.get('genderHightlighted'));
  },

  'click #btn-oreo-gender-swap': function(e, tpl) {
    e.preventDefault();

    if (!Session.equals('oreoLocked', true)) {
      var swappedText = Narek.Text.swapGenders(Narek.plainTextOreo.get());

      Narek.plainTextOreo.set(swappedText);
    }
    else {
      var swappedText = Narek.Text.swapGenders(Narek.plainTextKitkat.get());
    }

    Narek.plainTextKitkat.set(swappedText);
    Narek.History.saveState('swapGenders');
  },

  'click #btn-oreo-clear-breaks': function(e, tpl) {
    e.preventDefault();

    // Clears selection breaks and saves plainText
    Narek.Text.clearSelectionBreaks(function(result) {
      Narek.History.saveState('clearSelectionBreaks');
    });
  },

  'click #btn-oreo-break-sentences': function(e, tpl) {
    e.preventDefault();

    // Breaks text with newlines.
    Narek.Text.breakSelectedText(function(result) {
      // $('#window-oreo')[0].innerText = result;
      // Narek.Text.rescanSentences();
      Narek.History.saveState('breakSelectedText');
    });
  },

  'click #btn-oreo-collapse-sentences': function(e, tpl) {
    e.preventDefault();

    // Breaks text with newlines.
    Narek.Text.collapseSelectedText();
    Narek.History.saveState('collapseSelectedText');
  },

  'click #btn-oreo-display-original': function(e, tpl) {
    e.preventDefault();

    Session.set('displayOriginal', !Session.get('displayOriginal'));
  },

  'click #btn-oreo-save': function(e, tpl) {
    e.preventDefault();

    saveTextAsFile($('#window-oreo')[0].innerText, Session.get('loadedFileName') + '-OREO.txt');
  },

  'click #btn-oreo-rescan': function(e, tpl) {
    e.preventDefault();

    Narek.Text.rescanSentences();
  }
});
