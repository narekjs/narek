Template.display.helpers({
  'originalSentenceContent': function() {
    return Narek.originalSentenceContent.get();
  },

  'formattedSentenceContent': function() {
    return Narek.Autoswap.highlightOriginal();
  },

  'displayAutoswap': function() {
    return !!Session.get('displayAutoSwap');
  }
});
