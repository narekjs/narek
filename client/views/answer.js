Template.answer.helpers({
  duplicateCheck: function() {
    return Session.get('duplicateCheck');
  },
  differenceCheck: function() {
    return Session.get('differenceCheck');
  },
  answerIsIncorrect: function() {
    return Session.get('answerIsIncorrect');
  }
});

Template.answer.events({
  'click #btn-duplicate-check': function(e, tpl) {
    e.preventDefault();

    Session.set('sentenceMode', false);
    Session.set('differenceCheck', false);
    Session.set('oreoLocked', true);
    Session.toggle('duplicateCheck');

    if (Session.equals('kitkatChanged', true)) {
      Narek.plainTextKitkat.set($('#window-kitkat')[0].innerText);
    }
  },

  'click #btn-difference-check': function(e, tpl) {
    e.preventDefault();

    Session.set('sentenceMode', false);
    Session.set('duplicateCheck', false);
    Session.set('oreoLocked', true);
    Session.toggle('differenceCheck');
  },

  'click #btn-skeleton': function(e, tpl) {
    e.preventDefault();

    if (Narek.Kitkat.skeletonAdded !== true) {
      Narek.Helper.appendSkeleton();

      Narek.Kitkat.skeletonAdded = true;

      Narek.Text.rescanSentences(true);
      Narek.History.saveState('appendSkeleton');
      Session.set('sentenceMode', false);
      Session.set('oreoLocked', true);
    }
  },

  'click #btn-select-answer': function(e, tpl) {
    e.preventDefault();

    Narek.Answer.appendCorrectAnswer();

    Narek.Text.rescanSentences(true);
    Narek.History.saveState('appendCorrectAnswer');
    Session.set('sentenceMode', false);
    Session.set('oreoLocked', true);
  },

  'click #checkbox-answer-is-incorrect-wrap': function(e, tpl) {
    e.preventDefault();

    Session.set('answerIsIncorrect', !Session.get('answerIsIncorrect'));
    Narek.Answer.updateCorrectAnswer();
    Narek.History.saveState('updateCorrectAnswer');
  }
});
