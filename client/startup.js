Meteor.startup(function() {
  S.extendPrototype(); // Extend String prototype with String.js methods

  String.prototype.capitalizeFirstLetter = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
  }

  Options.set('showExportTab', true);

  Session.set('genderHightlighted', true);
  Session.set('displayAutoSwap', false);
  Session.set('displayOriginal', false);
  Session.set('displayVitals', false);
  Session.set('duplicateCheck', false);
  Session.set('differenceCheck', false);

  Session.set('answerIsIncorrect', true);
  Session.set('vitalsPulse', '');
  Session.set('vitalsBloodPressure', '');
  Session.set('vitalsRespiratoryRate', '');
  Session.set('vitalsTempC', '');
  Session.set('vitalsTempF', '');
  Session.set('vitalsO2', '');


  Session.set('loadedFolder', false);
  Session.set('dragAndDropActive', false);
  Session.set('genderSwapped', false);


  Session.set('honda-build-mode', false);
  Session.set('honda-cut-copy-mode', false);
});
