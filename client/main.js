insertTextAtCursor = function(text) {
    var sel, range, html;
    if (window.getSelection) {
        sel = window.getSelection();
        if (sel.getRangeAt && sel.rangeCount) {
            range = sel.getRangeAt(0);
            range.deleteContents();
            range.insertNode( document.createTextNode(text) );
        }
    } else if (document.selection && document.selection.createRange) {
        document.selection.createRange().text = text;
    }
}

/* Reads sentences from loaded text. */
readLines = function(text, context) {
  text = text.replace(/\r?\n|\r/g, '\n');
// console.log('readLines');
// console.log('text', text);
  var regexWholeCorrectAnswer = /(The correct answer is \([A-I]\) (?:[A-Za-z\d' ]*. )?\n{0,6}(?:(?:(?!Choice)[^\n]+[\n]{1,2})+)?)/g,
    wholeCorrectAnswer = text.match(regexWholeCorrectAnswer),
    regexWholeChoices;

  if (Narek.Kitkat.answerAdded === true) {
    regexWholeChoices = /((?:Choice [A-I] \(.*\)\ *)(?:(?:(?!Choice [A-I] \()[^\n]+[\n]{0,5})+)?)/g;
  }
  else {
    regexWholeChoices = /((?:Choice [A-I] \(.*\)\ *)(?:(?:(?!Choice [A-I] \()[^\n]+[\n]{1,2})+)?)/g;
  }

  var  wholeChoices = text.match(regexWholeChoices),
    regexWholeSkeleton = /((?:Cx:|Px:|Wx:|Tx:|PEARLS:)(?:(?:(?!Cx:|Px:|Wx:|Tx:|PEARLS:)[^\n]+[\n]{1,2})+)?\n{1,4})/g,
    wholeSkeleton = text.match(regexWholeSkeleton);

  if (context !== 'honda') {
    if (wholeCorrectAnswer) {
      $.each(wholeCorrectAnswer, function(key, value) {
        var replaced = value.replace(/\r?\n|\r/g, '<br>');

        replaced = replaced.replace(/<br>$/, '\n');
        text = text.replace(RegExp(escapeRegExp(value)), replaced);
      });
    }

    if (wholeChoices) {
// console.log('wholeChoices', wholeChoices);
// console.log('text', text);
      // Convert multiple line Answer block to one line.
      $.each(wholeChoices, function(key, value) {
        var replaced = value.replace(/\r?\n|\r/g, '<br>');
// console.log('value', value);
// console.log('replaced', replaced);
        text = text.replace(RegExp(escapeRegExp(value)), replaced);
      });
    }

    if (wholeSkeleton) {
      // Convert multiple line Skeleton block to one line.
      $.each(wholeSkeleton, function(key, value) {
        var replaced = value.replace(/\r?\n|\r/g, '<br>');

        replaced = replaced.replace(/<br>$/, '\n');
        replaced = replaced.replace(/<br>/g, '\n');
        text = text.replace(RegExp(escapeRegExp(value)), replaced);
      });
    }
  }

  var lines = [],
    sentenceNum = 0,
    match;
// console.log(text);
// console.log(text.split('\n'));
  $.each(text.split('\n'), function(key, value) {
// console.log('value', value);
    var wrapper = $('<div/>'),
      span = $('<span/>').attr({class: 'sentence'}),
      answerRegex = /^([A-I][.])[\w ]*[ \n]/g,
      correctAnswerRegex = /(The correct answer is \([A-I]\) *(?:[A-Za-z\d-: ,()'’]*. )?)((?:.*\s*)*)/g,
      answerChoiceRegex;

      if (Narek.Kitkat.answerAdded === true) {
        answerChoiceRegex = /(Choice [A-I] \(.*\)\ *(?:is incorrect. )*)((?:(?!Choice [A-I] \()[^\n]+[\n]{0,5})*)?/g;
      }
      else {
        answerChoiceRegex = /(Choice [A-I] \(.*\)\ *(?:is incorrect. )*)((?:(?!Choice [A-I] \()[^\n]+[\n]{1,2})*)?/g;
      }

      var topicHeadingRegex = /TOPIC HEADING/g,
      skeletonSentenceRegex = /(?:(Cx: |Px: |Wx: |Tx: |PEARLS: )\s*((?:(?!Cx:|Px:|Wx:|Tx:|PEARLS:)[^\n]+[\n]{1,2})+)?)/g,
      sentencesMatch = value.match(/[()A-Za-z\d\-\+\‘].+?($|[^\d][.!?]|([:]\s*\n))(\s*|$)/gm);
// console.log(value);
    // If is answer.
    if (value.match(answerRegex)) {
// console.log('1');
// console.log('value', value);
      sentenceNum++;
      span.attr('num', sentenceNum);
      span.attr('class', 'sentence answer');
      span.text(value.replace(/\r?\n|\r/g, ''));

      var line = wrapper.append(span).html();

      lines.push(line);
    }
    // If is correct answer.
    else if (value.match(correctAnswerRegex)) {
      value = value.replace(/<br>/g, '\n');
// console.log('2');
// console.log('value', value);
      if ((match = correctAnswerRegex.exec(value)) !== null) {
        sentenceNum++;
        var classes,
          line;

        switch (context) {
          case 'oreo':
            classes = 'sentence';
          break;

          case 'kitkat':
            classes = 'sentence' + ((Narek.Kitkat.answerAdded === true) ? ' answers-edit' : '');
          break;
        }

        span.attr('num', sentenceNum);
        span.attr('class', classes);
        span.attr('contenteditable', true);

        if (match[2] && match[2].replace(/\n$/, '') !== '' && $.trim(match[2]) !== '') {
// console.log('IF');
          span.text(match[2].replace(/\n$/, ''));
          lineClasses = 'correct-answer';
        }
        else {
// console.log('ELSE');
          if (context !== 'honda') {
            // span.text('\n\n\n\n');
          }
          else {
            //span.text('');
          }
          lineClasses = 'correct-answer inline';
        }

        // span.text(match[2].replace(/\r?\n|\r/g, '')));

        if (context === 'honda') {
          lines.push('<span class="sentence" num="' + sentenceNum + '">' + match[1] + '</span>');

          sentenceNum++;
          span.attr('num', sentenceNum);

          line = wrapper.append(span).html();

          if (span.text() !== '') {
            lines.push(line);
          }
        }
        else {
          line = wrapper.append(span).html();
          lines.push('<div class="' + lineClasses + '" contenteditable="false"><span contenteditable="false">' + match[1] + '</span>' + line + '<br></div>');
        }
      }
    }
    // If is answer choice.
    else if (value.match(answerChoiceRegex)) {
      var answerSelect = '';
// console.log('3');

      value = value.replace(/<br>/g, '\n') + '\n';
// console.log('value', value);
      while ((match = answerChoiceRegex.exec(value)) !== null) {
        if (match.index === answerChoiceRegex.lastIndex) {
            answerChoiceRegex.lastIndex++;
        }
// console.log('match', match);
        sentenceNum++;
        var classes,
          line,
          lineClasses;

        switch (context) {
          case 'oreo':
            classes = 'sentence';
          break;

          case 'kitkat':
            classes = 'sentence' + ((Narek.Kitkat.answerAdded === true) ? ' answers-edit' : '');
          break;
        }

        span.attr('num', sentenceNum);
        span.attr('class', classes);
        span.attr('contenteditable', true);

        if (match[2] && match[2].replace(/\n$/, '') !== '' && $.trim(match[2]) !== '') {
          span.text(match[2].replace(/\n$/, ''));
          lineClasses = 'choice';
        }
        else {
          if (context !== 'honda') {
            if (Narek.Kitkat.answerAdded === true) {
              // span.text('\n\n\n');
            }
          }
          else {
            //span.text('');
          }
          lineClasses = 'choice';
        }
        // span.text(match[2].replace(/\r?\n|\r/g, ''));

        if (context === 'honda') {
          lines.push('<span class="sentence" num="' + sentenceNum + '">' + match[1] + '</span>');

          sentenceNum++;
          span.attr('num', sentenceNum);

          line = wrapper.append(span).html();

          if (span.text() !== '') {
            lines.push(line);
          }
        }
        else {
          line = wrapper.append(span).html();

          answerSelect += '<div class="' + lineClasses + '" contenteditable="false"><span contenteditable="false">' + match[1] + '</span>' + line + '</div><br>';
        }
      }

      if (context !== 'honda') {
        lines.push(answerSelect);
      }
    }
    // If topic heading.
    else if ((match = topicHeadingRegex.exec(value)) !== null) {
      sentenceNum++;
// console.log('3-else');
      var classes,
        line;

      switch (context) {
        case 'oreo':
          classes = 'sentence topic-heading';
        break;

        case 'kitkat':
          classes = 'sentence topic-heading';

          if (Narek.Kitkat.skeletonAdded === true) {
            classes += ' skeleton-edit';
            span.attr('contenteditable', true);
            span.attr('onclick', '(this.textContent == \'TOPIC HEADING\') ? this.textContent = \'\' : this.textContent = this.textContent;');
          }
        break;
      }

      span.attr('num', sentenceNum);
      span.attr('class', classes);
      span.text(value.replace(/\r?\n|\r/g, ''));

      line = wrapper.append(span).html();

      lines.push(line);
    }
    // If is skeleton.
    else if (value.match(skeletonSentenceRegex)) {
      var skeleton = '';
// console.log('4-else');
      value = value.replace(/<br>/g, '\n') + '\n';

      while ((match = skeletonSentenceRegex.exec(value)) !== null) {
        if (match.index === skeletonSentenceRegex.lastIndex) {
            skeletonSentenceRegex.lastIndex++;
        }
// console.log(value);
        sentenceNum++;
        var classes,
          line,
          lineClasses;

        switch (context) {
          case 'oreo':
            classes = 'sentence';
          break;

          case 'kitkat':
            classes = 'sentence';

            if (Narek.Kitkat.skeletonAdded === true) {
              classes += ' skeleton-edit';
            }
          break;
        }

        span.attr('num', sentenceNum);
        span.attr('class', classes);
        span.attr('contenteditable', true);

        if (match[2]) {
          span.text(match[2].replace(/\n$/, ''));
          lineClasses = 'skeleton inline';
        }
        else {
          span.text('');
          lineClasses = 'skeleton';
        }
        // span.text(match[2].replace(/\r?\n|\r/g, ''));

        if (context === 'honda') {
          lines.push('<span class="sentence" num="' + sentenceNum + '">' + match[1] + '</span>');

          sentenceNum++;
          span.attr('num', sentenceNum);

          line = wrapper.append(span).html();

          if (span.text() !== '') {
            lines.push(line);
          }
        }
        else {
          line = wrapper.append(span).html();
          skeleton += '<div class="' + lineClasses + '" contenteditable="false"><span contenteditable="false">' + match[1] + '</span>' + line + '</div>'
        }
      }

      if (context !== 'honda') {
        lines.push(skeleton);
      }
    }
    // If is multiple sentences.
    else if (sentencesMatch !== null && sentencesMatch.length) {
      var sentences = '';
// console.log('5-else');
// console.log(sentencesMatch);
      $.each(sentencesMatch, function(k, value) {
        sentenceNum++;
        span.attr('num', sentenceNum);

        if (value.trim() != '') {
          span.text(value.replace(/\r?\n|\r/g, ''));
          sentences += wrapper.append(span).html();
        }
      });
// console.log(sentences);
      lines.push(sentences);
    }
    else {
      span.text(value.replace(/\r?\n|\r/g, ''));
// console.log('6-else');
      if (value.trim() != '') {
        sentenceNum++;
        span.attr('num', sentenceNum);

        var line = wrapper.append(span).html();
      }
      else {
        // This line creates empty span
        if (Session.get('dragAndDropActive') === true || context == 'honda') {
          var emptyLineClasses = 'empty-line';

          if (Session.get('honda-build-mode') === true) {
            emptyLineClasses += ' inline-block';
            span.text('\xa0');
          }

          span.attr('class', emptyLineClasses);

          var line = wrapper.append(span).html();
        }
      }

      lines.push(line);
    }
  });

  Session.set('sentenceCount', sentenceNum);

  return lines;
};

getSelectionParentElementId = function() {
  var parentEl = null, selection, id;

  if (window.getSelection) {
    selection = window.getSelection();

    if (selection.rangeCount > 0) {
      var selectionContainer = selection.getRangeAt(0).commonAncestorContainer;

      if (selectionContainer.className != 'answers') {
        // If Node is not element but text (maybe).
        if (selectionContainer.nodeType != 1) {
          if (selectionContainer.parentElement.parentElement.id == 'window-oreo-wrap' || selectionContainer.parentElement.parentElement == 'window-kitkat-wrap') {
            parentEl = selectionContainer.parentElement;
          }
          else {
            parentEl = selectionContainer.parentElement.parentElement;
          }
        }
        else {
          if (selectionContainer.id) {
            if (selectionContainer.id == 'window-oreo' || selectionContainer.id == 'window-kitkat') {
              parentEl = selectionContainer;
            }
            else {
              switch (selectionContainer.id) {
                case 'window-oreo-wrap':
                  parentEl = selectionContainer.querySelector('#window-oreo');
                break;

                case 'window-kitkat-wrap':
                  parentEl = selectionContainer.querySelector('#window-kitkat');
                break;

                default:
                  parentEl = selectionContainer;
              }
            }
          }
          else {
            parentEl = selectionContainer.parentElement;
          }
        }
      }
      else {
        // Container is an answers block.
        parentEl = selectionContainer.parentElement;
      }
    }
  }
  else if ((selection = document.selection) && selection.type != "Control") {
    parentEl = selection.createRange().parentElement();
  }

  return parentEl.id;
};

/* Escapes string for use in regex. */
escapeRegExp = function(string) {
  return string.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
};

/* Highlights text as selected in given element. */
selectText = function(element) {
  Meteor.setTimeout(
    function() {
      var doc = document,
        text = doc.getElementById(element),
        range,
        selection;

      if (doc.body.createTextRange) { //ms
        range = doc.body.createTextRange();
        range.moveToElementText(text);
        range.select();
      }
      else if (window.getSelection) { //all others
        selection = window.getSelection();
        range = doc.createRange();
        range.selectNodeContents(text);
        selection.removeAllRanges();
        selection.addRange(range);
      }
    },
    100
  );
};

highlightSelectedOreoSentence = function() {
  Meteor.setTimeout(
    function() {
      // Highlight next sentence, if is
      $('#layout-top span').removeClass('sentence-highlighted');

      if (Session.equals('oreoLocked', true)) {
        $('#window-oreo span[num=' + Narek.selectedSentenceId.get() + ']').addClass('sentence-highlighted');
      } else {
        $('#layout-top span[num=' + Narek.selectedSentenceId.get() + ']').addClass('sentence-highlighted');
      }
    },
    100
  );
};

highlightSelectedKitkatSentence = function() {
  Meteor.setTimeout(
    function() {
      // Highlight next sentence, if is
      $('#layout-top span').removeClass('sentence-highlighted');

      if (Session.equals('oreoLocked', true)) {
        $('#window-kitkat span[num=' + Narek.selectedSentenceId.get() + ']').addClass('sentence-highlighted');
      } else {
        $('#layout-top span[num=' + Narek.selectedSentenceId.get() + ']').addClass('sentence-highlighted');
      }
    },
    100
  );
};

saveTextAsFile = function(textToWrite, fileName)
{
  if (textToWrite) {
    // textToWrite = textToWrite.replace(/\r?\n/g, '\r');

    var textFileAsBlob = new Blob([textToWrite], {type:'text/plain'}),
      downloadLink = document.createElement('a');

    downloadLink.download = fileName;
    downloadLink.innerHTML = 'Download File';

    function destroyClickedElement(event)
    {
      document.body.removeChild(event.target);
    }

    if (window.URL != null)
    {
      // Firefox requires the link to be added to the DOM
      // before it can be clicked.
      downloadLink.href = window.URL.createObjectURL(textFileAsBlob);
      downloadLink.onclick = destroyClickedElement;
      downloadLink.style.display = 'none';
      document.body.appendChild(downloadLink);
    }

    downloadLink.click();
  }
};
