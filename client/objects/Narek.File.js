/* Narek File object. */
Narek.File = {
  /* Read file and call the callback on success. */
  readFile: function(file, callback) {
    var reader  = new FileReader();

    reader.onload = function(event) {
      var result = event.target.result;

      callback(result);
    };

    reader.readAsText(file, 'ISO-8859-15');
  },

  /* Loads file content as Oreo and Kitkat text. */
  loadSelectedFile: function(result) {
    Narek.History.reset();
    Narek.Helper.setGenderRadioButton(result);

    if (Narek.plainTextOreo.get() !== result) {
      if ($('#window-oreo')[0].innerText != '') { // TODO: Remove check?
        $('#window-oreo')[0].innerText = '';
      }

      Narek.plainTextOreo.set(result);
      Narek.plainTextKitkat.set(result);

      Session.set('originalText', result);

      Narek.Sentence.resetSelectedSentenceVariables();

      Session.set('oreoLocked', false);
      Session.set('dragAndDropActive', false);
      Session.set('genderSwapped', false);
      Session.set('sentenceMode', true);

      Narek.Kitkat.answerAdded = false;
      Narek.Kitkat.skeletonAdded = false;

      Narek.History.saveState('loadSelectedFile');
    }
  },

  /* Loads file content as Oreo text. */
  loadSelectedFileOreo: function(result) {
    if ($('#window-oreo')[0].innerText != '') { // TODO: Remove check?
      $('#window-oreo')[0].innerText = '';
    }

    Narek.plainTextOreo.set(result);

    Session.set('originalText', result);
    Session.set('oreoLocked', true);
  },

  /* Loads file content as Kitkat text. */
  loadSelectedFileKitkat: function(result) {
    Narek.Helper.setGenderRadioButton(result);

    if ($('#window-kitkat')[0].innerText != '') { // TODO: Remove check?
      // $('#window-kitkat')[0].innerText = '';
    }

    Narek.plainTextKitkat.set(result);
    Session.set('oreoLocked', true);
    Session.set('genderSwapped', false);
  }
};
