/* Narek History object */
Narek.History = {
  init: function() {
    var initialState = {};

    this.plainTextOreo = '';
    this.plainTextKitkat = '';
    this.selectedSentenceId = null;
    this.selectedSentenceSource = '';
    this.selectedSentenceContent = '';
    this.originalSentenceContent = '';
    this.activeState = 0;
    this.states = [];

    initialState.plainTextOreo = '';
    initialState.plainTextKitkat = '';
    initialState.selectedSentenceId = null;
    initialState.selectedSentenceSource = '';
    initialState.selectedSentenceContent = '';
    initialState.originalSentenceContent = '';

    this.states.push(initialState);
  },
  reset: function() {
    this.init();
  },

  saveState: function(action) {
    var currentState = Narek.History.getCurrentState();

    currentState.action = action;

    if (this.activeState != this.states.length - 1) {
      this.states = this.states.slice(0, this.activeState + 1);
    }

    this.states.push(currentState);
    this.activeState++;
  },

  getCurrentState: function(action) {
    var state = {};

    state.plainTextOreo = Narek.plainTextOreo.get();

    state.plainTextKitkat = Narek.plainTextKitkat.get();

    state.selectedSentenceId = Narek.selectedSentenceId.get();
    state.selectedSentenceSource = Narek.selectedSentenceSource.get();
    state.selectedSentenceContent = Narek.selectedSentenceContent.get();
    state.originalSentenceContent = Narek.originalSentenceContent.get();

    state.answerAdded = Narek.Kitkat.answerAdded;
    state.skeletonAdded = Narek.Kitkat.skeletonAdded;

    return state;
  },

  loadState: function(indexState) {
    var state = this.states[indexState];

    Narek.Kitkat.answerAdded = state.answerAdded;
    Narek.Kitkat.skeletonAdded = state.skeletonAdded;

    Narek.plainTextOreo.set(state.plainTextOreo);
    Narek.plainTextKitkat.set(state.plainTextKitkat);
    Narek.selectedSentenceId.set(state.selectedSentenceId);
    Narek.selectedSentenceSource.set(state.selectedSentenceSource);
    Narek.selectedSentenceContent.set(state.selectedSentenceContent);
    Narek.originalSentenceContent.set(state.originalSentenceContent);

  },

  undo: function() {
    var previousState = this.activeState - 1;

    // Load previous state.
    if (this.activeState > 0) {
      this.loadState(previousState);
      this.activeState--;
    }
    else {
      console.log('previousState: ' + previousState);
    }
  },
  redo: function() {
    var nextState = this.activeState + 1;

    // Load next state.
    if (this.states[nextState]) {
      this.loadState(nextState);
      this.activeState++;
    }
    else {
      console.log('nextState: ' + nextState);
      console.log(this.states[nextState]);
    }
  }
};

Narek.History.init();
