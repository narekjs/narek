/* Narek Kitkat object */
Narek.Kitkat = {
  answerAdded: false,
  skeletonAdded: false,

  buildSentences: function(plainTextKitkat) {
    if ((Narek.Kitkat.answerAdded === true || Narek.Kitkat.skeletonAdded === true) && $('#window-kitkat')[0]) {
      textDisplayKitkat = $('#window-kitkat')[0].innerText || plainTextKitkat;
    }
    else {
      textDisplayKitkat = plainTextKitkat;
    }

    if (textDisplayKitkat) {
      var lines = readLines(textDisplayKitkat, 'kitkat');
      textDisplayKitkat = lines.join('<br>');

      // Set to be available in duplicate check.
      Session.set('textDisplayKitkat', textDisplayKitkat);

      if (Session.get('duplicateCheck')) {
        textDisplayKitkat = Narek.Text.highlightDuplicates(textDisplayKitkat);
      }

      if (Session.get('differenceCheck')) {
        textDisplayKitkat = Narek.Kitkat.highlightDifferences(textDisplayKitkat);
      }

      if (!Session.get('duplicateCheck') && !Session.get('differenceCheck') && Session.get('genderHightlighted')) {
        textDisplayKitkat = Narek.Text.highlightGender(textDisplayKitkat);
      }
    }

    if (Narek.selectedSentenceId.get() !== null && Narek.selectedSentenceSource.get() == 'kitkat') {
      highlightSelectedKitkatSentence();
    }

    return ((textDisplayKitkat) ? textDisplayKitkat : '');
  },

  highlightDifferences: function(textKitkat) {
    // Gets all kitkat sentences and its span elements
    var sentencesKitkat = textKitkat.match(/<span[^>]+?.*?>([\s\S]*?)<\/span>/g),
      textOreo = Session.get('textDisplayOreo'),
      wordsRegex = /(\S+\s*)/g;

    $.each(sentencesKitkat, function(keySentenceKitkat, sentenceKitkat) {
      var $sentenceKitkat = $(sentenceKitkat),
        textSentence = $sentenceKitkat.text(),
        lastWasDifference = false,
        lastTwoWasDifference = false;

      if (textSentence.trim() != '') {
        var wordsKitkat = textSentence.match(wordsRegex),
          wordsDifferences = [];

        for (var i = 0; i < wordsKitkat.length; i++) {
          if (!wordsKitkat[i + 2]) {
            break;
          }
          else {
            if (textOreo.toLowerCase().indexOf(wordsKitkat[i].toLowerCase() + wordsKitkat[i + 1].toLowerCase() + wordsKitkat[i + 2].toLowerCase()) > -1) {
              wordsDifferences[i] = wordsKitkat[i];
              wordsDifferences[i + 1] = wordsKitkat[i + 1];
              wordsDifferences[i + 2] = wordsKitkat[i + 2];

              lastTwoWasDifference = true;
            }
            else {
              if (lastTwoWasDifference === true) {
                wordsDifferences[i + 2] = '<span class="difference">' + wordsKitkat[i + 2] + '</span>';

                lastTwoWasDifference = false;
                lastWasDifference = true;
              }
              else if (lastWasDifference === true) {
                wordsDifferences[i + 1] = '<span class="difference">' + wordsKitkat[i + 1] + '</span>';
                wordsDifferences[i + 2] = '<span class="difference">' + wordsKitkat[i + 2] + '</span>';

                lastWasDifference = false;
              }
              else {
                wordsDifferences[i] = '<span class="difference">' + wordsKitkat[i] + '</span>';
                wordsDifferences[i + 1] = '<span class="difference">' + wordsKitkat[i + 1] + '</span>';
                wordsDifferences[i + 2] = '<span class="difference">' + wordsKitkat[i + 2] + '</span>';
              }
            }
          }
        }

        if (wordsDifferences.length !== 0) {
          textSentence = wordsDifferences.join('');
          textKitkat = textKitkat.replace(textKitkat.match(escapeRegExp($sentenceKitkat[0].outerHTML)), $sentenceKitkat.html(textSentence)[0].outerHTML);
        }
      }
    });

    return textKitkat;
  }
};
