/* Narek Helper object */
Narek.Helper = {
  toggleSentenceMode: function() {
    Session.set('duplicateCheck', false);
    Session.set('differenceCheck', false);
    Session.set('sentenceMode', !Session.get('sentenceMode'));
  },

  toggleDragAndDrop: function() {
    Session.set('dragAndDropActive', !Session.get('dragAndDropActive'));

    var dragAndDropActive = Session.get('dragAndDropActive');

    if (dragAndDropActive === true) {
      // Enable sentences drag & drop.
      Meteor.setTimeout(function() {
        Narek.Helper.initDragAndDrop();
      }, 100);
    }
    else {
      // Disable sentences drag & drop.
      Narek.Helper.deInitDragAndDrop();
    }
  },

  toggleAutoSwap: function() {
    var isOn = Session.get('displayAutoSwap');
    if (!isOn) {
      progressJs().start();
      Session.set('sentenceMode', true);
      Session.set('oreoLocked', true);
      var selected = Narek.selectedSentenceId.get();

      if (selected === null) {
        $('#window-kitkat span[num=1]').trigger('click');
      }
      else {
        $('#window-kitkat span[num=' + selected + ']').trigger('click');
      }

      Meteor.setTimeout(function(){
        Session.set('displayAutoSwap', true);
        Narek.Autoswap.generateSentences();
      }, 100);
    }
    else {
      Session.set('displayAutoSwap', false);
      Session.set('sentenceMode', false);
    }
  },

  setGenderRadioButton: function(content) {
    var gender = Narek.Autoswap.resolveGender(content);

    $('input[name=gender-radios]').parent().removeClass('active');

    switch (gender) {
      case 'male':
         $('#gender-radio-buttons-group input[value=male]').parent().addClass('active')
      break;

      case 'female':
         $('#gender-radio-buttons-group input[value=female]').parent().addClass('active')
      break;
    }
  },

  deleteAnswers: function(callback) {
    if (Narek.Kitkat.answerAdded === true && $('#window-kitkat .selected-answer').length !== 0) {
      $('#window-kitkat .selected-answer').remove();
      // callback();
    }
    else {
      $('#window-kitkat .correct-answer').remove();
      $('#window-kitkat .choice').remove();
      // callback();
    }
  },

  deleteSkeleton: function() {
    $('#window-kitkat .topic-heading').remove();
    $('#window-kitkat .skeleton').remove();
  },

  insertHtmlAtCaret: function(html, source) {
    var appendAfterLastBlock = function() {
      if (source == 'skeleton' && $('.selected-answer').length !== 0) {
        $('#window-kitkat .selected-answer').after('<br><br>' + html);
      }
      else if (source == 'skeleton' && $('.correct-answer').length !== 0) {
        $('#window-kitkat .choice').last().after('<br><br>' + html);
      }
      else {
        var windowKitkatAnswer = $('#window-kitkat .answer');

        if (windowKitkatAnswer.length !== 0) {
          windowKitkatAnswer.last().after('<br><br>' + html);
        }
        else {
          $('#window-kitkat').append(html);
        }
      }
    };

    if (window.getSelection) {
      // IE9 and non-IE
      var selection = window.getSelection();
      if (selection.baseNode !== null) {
        if (selection.baseNode.id == 'window-kitkat') {
          if (selection.rangeCount > 0) {
            var range = selection.getRangeAt(0),
              fragment = range.createContextualFragment(html)

            range.deleteContents();
            range.insertNode(fragment);
          }
          else {
            appendAfterLastBlock();
          }
        }
        else {
          appendAfterLastBlock();
        }
      }
      else {
        appendAfterLastBlock();
      }
    }
  },

  appendSkeleton: function() {
      var skeletonTemplate = [
        '<span class="skeleton"><span class="skeleton-edit" contenteditable="true" onclick="(this.textContent == \'TOPIC HEADING\') ? this.textContent = \'\' : this.textContent = this.textContent;">TOPIC HEADING</span></span>',
        '<span class="skeleton"><span>Cx: </span><span class="skeleton-edit" contenteditable="true"></span></span>',
        '<span class="skeleton"><span>Px: </span><span class="skeleton-edit" contenteditable="true"></span></span>',
        '<span class="skeleton"><span>Wx: </span><span class="skeleton-edit" contenteditable="true"></span></span>',
        '<span class="skeleton"><span>Tx: </span><span class="skeleton-edit" contenteditable="true"></span></span>',
        '<span class="skeleton"><span>PEARLS: </span><span class="skeleton-edit" contenteditable="true"></span></span>'
      ];

    // Narek.Helper.insertHtmlAtCaret(skeletonTemplate.join('<br>'), 'skeleton');
    if ($('#window-kitkat .skeleton').length === 0) {
      Narek.Helper.insertHtmlAtCaret('<div class="skeleton-block" contenteditable="false">' + skeletonTemplate.join('<br>') + '</div><br>', 'skeleton');
    }
  },

  dragOnStop: function(event, ui) {
    var previousSibling = ui.item[0].previousSibling;

    if (previousSibling && !/\s+$/.test(previousSibling.textContent)) {
      previousSibling.textContent += ' ';
    }
  },

  initDragAndDrop: function() {
    Session.set('oreoLocked', true);

    var options = {
        placeholder: 'ui-state-error',
        cursor: 'move',
        opacity: 0.01,
        // items: '> span',
        stop: Narek.Helper.dragOnStop,
        placeholder: {
          element: function(e, ui) {
              return $('<div style="border: 1px solid red"><br>' + e[0].innerHTML + '<br><br></div>');
          },
          update: function() {
              return;
          }
        }
      };

    if ($('#window-oreo').sortable('instance')) {
      $('#window-oreo').sortable('enable');
    }
    else {
      options.connectWith = ['.answers', '.selected-answer', '#window-kitkat'];
      $('#window-oreo').sortable(options);
    }

    if ($('#window-kitkat').sortable('instance')) {
      $('#window-kitkat').sortable('enable');
    }
    else {
      options.connectWith = ['.answers', '.selected-answer', '#window-oreo'];
      $('#window-kitkat').sortable(options);
    }

    if ($('.answers').sortable('instance')) {
      $('.answers').sortable('enable');
    }
    else {
      options.connectWith = ['#window-oreo', '#window-kitkat'];
      $('.answers').sortable(options);
    }

    if ($('.selected-answer').sortable('instance')) {
      $('.selected-answer').sortable('enable');
    }
    else {
      options.connectWith = ['#window-oreo', '#window-kitkat'];
      $('.selected-answer').sortable(options);
    }

    $('[contenteditable]').attr('contenteditable', false);
    Session.set('sentenceMode', false);
    $('.empty-line').css({'width': '100%', 'display': 'inline-block'});
    $('.empty-line').text('\xa0');
  },

  deInitDragAndDrop: function() {
    if ($('#window-kitkat').sortable('instance')) {
      $('#window-oreo').sortable('disable');
      $('#window-kitkat').sortable('disable');
      $('.answers').sortable('disable');
      $('.selected-answer').sortable('disable');
    }

    $('[contenteditable]').attr('contenteditable', true);
    Session.set('sentenceMode', true);
    $('.empty-line').css('');
    $('.empty-line').text('');

    Session.set('oreoChanged', true);
    Session.set('kitkatChanged', true);
    Narek.Text.rescanSentences();
  }

};
