/* Narek Text object */
Narek.Text = {
  getWholeSentences: function(text) {
    var sentences = [],
      regexWholeSentences = /([()A-Za-z\d].+?(?:$|[^\d][.!?]|([:]\s*\n))(?:\s*|$))/gm;

    // Replace all newline symbols.
    text = text.replace(/\n/g, "");

    // Match whole sentences.
    sentences = text.match(regexWholeSentences);
    sentences = Narek.Sentence.verify(sentences);

    return sentences;
  },

  // Gets words from text.
  getWordsFromText: function(text) {
    var regex = /(\w+)/g,
      match,
      words = [];

    while ((match = regex.exec(text)) !== null) {
      if (match.index === regex.lastIndex) {
          regex.lastIndex++;
      }

      words.push(match[0]);
    }

    return words;
  },

  readSentencesForHonda: function(text) {
    text = text.replace(/\r\n?/g, '\n');

    var regexHondaBlocks = /((?:.|\n)*?)\n\n((?:(?:(?:[A-I]\. .*\n*))+)*)\n*((?:.|\n)*)/,
      match = regexHondaBlocks.exec(text),
      // regexAnswers = /^([A-I][.])[\w ]*[ \n]/g;
      sentences = [],
      order = 0;

    if (match !== null) {
      if (match.index === regexHondaBlocks.lastIndex) {
        regexHondaBlocks.lastIndex++;
      }

      // Before answers block
      if (match[1]) {
        var wholeSentences = this.getWholeSentences(match[1]);

        wholeSentences.forEach(function(sentence) {
          order++;

          sentences.push({
            text: sentence,
            order: order
          });
        });
      }

      // Answers block
      if (match[2]) {
        // Remove newlines from the end and add to the sentences array.
        order++;

        sentences.push({
          text: match[2].replace(/\n+$/, ''),
          order: order
        });
      }

      // After answers block
      if (match[3]) {
        var wholeSentences = this.getWholeSentences(match[3]);

        wholeSentences.forEach(function(sentence) {
          order++;

          sentences.push({
            text: sentence,
            order: order
          });
        });
      }
    }

    return sentences;
  },

  rescanSentences: function(saveAddedState, callback) {
    Session.set('fullRescan', true);

    if (Session.equals('oreoChanged', true)) {

      // RESCAN OREO SENTENCES
      var textOreo = document.getElementById('window-oreo').innerText;

      Narek.plainTextOreo.set(textOreo);
      Session.set('oreoChanged', false);
    }

    if (Session.equals('kitkatChanged', true) || (Narek.Kitkat.answerAdded === true) || Narek.Kitkat.skeletonAdded === true) {
      // RESCAN KITKAT SENTENCES
      var textKitkat = document.getElementById('window-kitkat').innerText;

      Narek.plainTextKitkat.set(textKitkat);

      Session.set('kitkatChanged', false);

      if (!saveAddedState) {
        Narek.Kitkat.answerAdded = false;
        Narek.Kitkat.skeletonAdded = false;
      }
    }

    if (Narek.Honda.buildFileChanged.get() === true) {
      var textBuildFile = document.getElementById('window-honda-build').innerText;

      Narek.Honda.buildFile.set(textBuildFile);
      Narek.Honda.buildFileChanged.set(false);
    }

    if (callback) {
      callback();
    }
  },

  /* Clears breaks from selection. */
  clearSelectionBreaks: function(callback) {
    var selection = document.getSelection();

    if (selection.rangeCount > 0) {
      var selectionString = selection.toString(),
        breaksCleaned = selectionString.replace(/\r?\n|\r/g, ''),
        parentId = getSelectionParentElementId();

      if ($.trim(selectionString) !== '') {
        switch (parentId) {
          case 'window-oreo':
            var result = $('#window-oreo')[0].innerText.replace(selectionString, breaksCleaned);

            Narek.plainTextOreo.set(result);

            if (!Session.equals('oreoLocked', true)) {
              Narek.plainTextKitkat.set(result);
            }
          break;

          case 'window-kitkat':
            var result = $('#window-kitkat')[0].innerText.replace(selectionString, breaksCleaned);

            Narek.plainTextKitkat.set(result);

            if (!Session.equals('oreoLocked', true)) {
              Narek.plainTextOreo.set(result);
            }
          break;

          default:
            var sourceContent = document.getElementById(parentId).querySelector('.source-content'),
              result = sourceContent.innerText.replace(selectionString, breaksCleaned),
              dataObject = Narek.Honda.loadedFilesCollection.findOne({windowId: parentId});

            Narek.Honda.upsertHondaSourcePlainText(result, dataObject._id, dataObject);
        }

        if (callback && result) {
          callback(result);
        }
      }
    }
    else {
      console.log('Text is not selected!');
    }
  },

  /* Breaks selected text with newlines */
  breakSelectedText: function(callback) {
    var selection = document.getSelection();

    if (selection.rangeCount > 0) {
      var selectionString = selection.toString(),
        regexWholeSentences = /([()A-Za-z\d].+?(?:$|[^\d][.!?]|([:]\s*\n))(?:\s*|$))/gm,
        breaksCreated = selectionString.replace(/\n/g, "").replace(regexWholeSentences, "$1\n\n"),
        parentId = getSelectionParentElementId();

      if (selectionString) {
        switch (parentId) {
          case 'window-oreo':
            var result = $('#window-oreo')[0].innerText.replace(selectionString, breaksCreated);

            Narek.plainTextOreo.set(result);

            if (!Session.equals('oreoLocked', true)) {
              Narek.plainTextKitkat.set(result);
            }
          break;

          case 'window-kitkat':
            var result = $('#window-kitkat')[0].innerText.replace(selectionString, breaksCreated);

            Narek.plainTextKitkat.set(result);

            if (!Session.equals('oreoLocked', true)) {
              Narek.plainTextOreo.set(result);
            }
          break;

          default:
            var sourceContent = document.getElementById(parentId).querySelector('.source-content'),
              result = sourceContent.innerText.replace(selectionString, breaksCreated),
              dataObject = Narek.Honda.loadedFilesCollection.findOne({windowId: parentId});

            Narek.Honda.upsertHondaSourcePlainText(result, dataObject._id, dataObject);
        }

        if (callback && result) {
          callback(result);
        }
      }
    }
    else {
      console.log('Text is not selected!');
    }
  },

  /* Replaces multiple newlines with single one in selected text */
  collapseSelectedText: function() {
    var selection = document.getSelection();

    if (selection.rangeCount > 0) {
      var selectionString = selection.toString(),
        regexWholeSentences = /([()A-Za-z\d].+?(?:$|[^\d][.!?]|([:]\s*\n))(?:\s*|$))/gm,
        collapsedText = selectionString.replace(/\n/g, "").replace(regexWholeSentences, "$1\n"),
        parentId = getSelectionParentElementId();

      if (selectionString) {
        switch (parentId) {
          case 'window-oreo':
            var result = $('#window-oreo')[0].innerText.replace(selectionString, collapsedText);

            Narek.plainTextOreo.set(result);

            if (!Session.equals('oreoLocked', true)) {
              Narek.plainTextKitkat.set(result);
            }
          break;

          case 'window-kitkat':
            var result = $('#window-kitkat')[0].innerText.replace(selectionString, collapsedText);

            Narek.plainTextKitkat.set(result);

            if (!Session.equals('oreoLocked', true)) {
              Narek.plainTextOreo.set(result);
            }
          break;

          default:
            var sourceContent = document.getElementById(parentId).querySelector('.source-content'),
              result = sourceContent.innerText.replace(selectionString, collapsedText),
              dataObject = Narek.Honda.loadedFilesCollection.findOne({windowId: parentId});

            Narek.Honda.upsertHondaSourcePlainText(result, dataObject._id, dataObject);
        }
      }
    }
    else {
      console.log('Text is not selected!');
    }
  },

  clearDoubleSpaces: function() {
    var selection = document.getSelection();

    if (selection.rangeCount > 0) {
      var selectionString = selection.toString(),
        // Replace all newlines to prevent that '\s' recognizes them as space, then replace all spaces (\s) with one space and the replace back all newlines
        spacesRemoved = selectionString.replace(/\n/g, 'newline').replace(/\s\s+/g, ' ').replace(/newline/g, '\n'),
        parentId = getSelectionParentElementId();

      // Remove space from the beginning of the sentence.
      spacesRemoved = spacesRemoved.replace(/^\s/, '');

      if ($.trim(selectionString) !== '' && parentId) {
        switch (parentId) {
          case 'window-oreo':
            var result = $('#window-oreo')[0].innerText.replace(selectionString, spacesRemoved);

            Narek.plainTextOreo.set(result);

            if (!Session.equals('oreoLocked', true)) {
              Narek.plainTextKitkat.set(result);
            }
          break;

          case 'window-kitkat':
            var result = $('#window-kitkat')[0].innerText.replace(selectionString, spacesRemoved);

            Narek.plainTextKitkat.set(result);

            if (!Session.equals('oreoLocked', true)) {
              Narek.plainTextOreo.set(result);
            }
          break;

          default:
            var sourceContent = document.getElementById(parentId).querySelector('.source-content'),
              result = sourceContent.innerText.replace(selectionString, spacesRemoved),
              dataObject = Narek.Honda.loadedFilesCollection.findOne({windowId: parentId});

            Narek.Honda.upsertHondaSourcePlainText(result, dataObject._id, dataObject);
        }
      }
    }
    else {
      console.log('Text is not selected!');
    }
  },

  /* Swaps genders. */
  swapGenders: function(text, source) {
    var words = Narek.Text.getWordsFromText(text);

    $.each(words, function(key, word) {
      GenderPronouns.find().forEach(function(pronoun) {
        // Male pronouns.
        if (word == pronoun.malePronoun) {
          text = text.replace(RegExp('\\b' + word + '\\b', 'g'), pronoun.femalePronoun);
        }
        // Capitalized male pronouns.
        else if (word == s(pronoun.malePronoun).capitalize().value()) {
          text = text.replace(RegExp('\\b' + word + '\\b', 'gi'), s(pronoun.femalePronoun).capitalize().value());
        }
        // Female pronouns.
        else if (word == pronoun.femalePronoun) {
          text = text.replace(RegExp('\\b' + word + '\\b', 'g'), pronoun.malePronoun);
        }
        // Capitalized female pronouns.
        else if (word == s(pronoun.femalePronoun).capitalize().value()) {
          text = text.replace(RegExp('\\b' + word + '\\b', 'gi'), s(pronoun.malePronoun).capitalize().value());
        }
      });
    });

    if (source && source !== 'honda') {
      Session.set('genderSwapped', true);

      Narek.Helper.setGenderRadioButton(text);
    }

    return text.capitalizeFirstLetter();
  },

  /* Highlights genders in given text. */
  highlightGender: function(text) {
    var pronouns = [],
      genderSwapped = Session.get('genderSwapped');

    if (text) {
      GenderPronouns.find().forEach(function(pronoun) {
        if (pronoun.malePronoun == 'his') {
          if (genderSwapped) {
            text = text.replace(RegExp('(\\b' + pronoun.malePronoun + '\\b)', 'gi'), '<span class="genderAmbiguous">$1</span>');
          }
          else {
            text = text.replace(RegExp('(\\b' + pronoun.malePronoun + '\\b)', 'gi'), '<span class="genderMale">$1</span>');
          }
        }
        else if (pronoun.femalePronoun == 'her') {
          if (genderSwapped) {
            text = text.replace(RegExp('(\\b' + pronoun.femalePronoun + '\\b)', 'gi'), '<span class="genderAmbiguous">$1</span>');
          }
          else {
            text = text.replace(RegExp('(\\b' + pronoun.femalePronoun + '\\b)', 'gi'), '<span class="genderFemale">$1</span>');
          }
        }
        else {
          text = text.replace(RegExp('(\\b' + pronoun.malePronoun + '\\b)', 'gi'), '<span class="genderMale">$1</span>');
          text = text.replace(RegExp('(\\b' + pronoun.femalePronoun + '\\b)', 'gi'), '<span class="genderFemale">$1</span>');
        }
      });
    }

    return text;
  },

  /* wraps duplicates for highlighting. */
  highlightDuplicates: function(sourceText, source) {
    // Gets all sentences
    var sourceSentences = sourceText.match(/<span[^>]+?.*?>([\s\S]*?)<\/span>/g),
      wordsRegex = /(\S+\s*)/g,
      textToCompare;

    if (source === 'oreo') {
      textToCompare = Session.get('textDisplayKitkat');
    }
    else {
      textToCompare = Session.get('textDisplayOreo');
    }

    $.each(sourceSentences, function(key, sourceSentence) {
      var $sourceSentence = $(sourceSentence),
        sentenceText = $sourceSentence.text(),
        lastWasDuplicate = false,
        lastTwoWasDuplicate = false;

      if (sentenceText.trim() != '') {
        var sentenceWords = sentenceText.match(wordsRegex),
          wordsDuplicates = [];

        for (var i = 0; i < sentenceWords.length; i++) {
          if (!sentenceWords[i + 2]) {
            break;
          }
          else {
            if (textToCompare.toLowerCase().indexOf(sentenceWords[i].toLowerCase() + sentenceWords[i + 1].toLowerCase() + sentenceWords[i + 2].toLowerCase()) > -1) {
              wordsDuplicates[i] = '<span class="duplicate">' + sentenceWords[i] + '</span>';
              wordsDuplicates[i + 1] = '<span class="duplicate">' + sentenceWords[i + 1] + '</span>';
              wordsDuplicates[i + 2] = '<span class="duplicate">' + sentenceWords[i + 2] + '</span>';
              lastTwoWasDuplicate = true;
            }
            else {
              if (lastTwoWasDuplicate === true) {
                wordsDuplicates[i + 2] = sentenceWords[i + 2];

                lastTwoWasDuplicate = false;
                lastWasDuplicate = true;
              }
              else if (lastWasDuplicate === true) {
                wordsDuplicates[i + 1] = sentenceWords[i + 1];
                wordsDuplicates[i + 2] = sentenceWords[i + 2];

                lastWasDuplicate = false;
              }
              else {
                wordsDuplicates[i] = sentenceWords[i];
                wordsDuplicates[i + 1] = sentenceWords[i + 1];
                wordsDuplicates[i + 2] = sentenceWords[i + 2];
              }
            }
          }
        }

        if (wordsDuplicates.length !== 0) {
          sentenceText = wordsDuplicates.join('');
          var textMatch = sourceText.match(escapeRegExp($sourceSentence[0].outerHTML));
          var $sourceSentenceOuterHTML = $sourceSentence.html(sentenceText)[0].outerHTML;
// console.log('textMatch', textMatch);
// console.log('$sourceSentenceOuterHTML', $sourceSentenceOuterHTML);
          sourceText = sourceText.replace(textMatch, $sourceSentenceOuterHTML);
        }
      }
    });

    return sourceText;
  }
};
