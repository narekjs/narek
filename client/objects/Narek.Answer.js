/* Narek Answer object */
Narek.Answer = {
  /* Cleans choices in selected text */
  cleanChoices: function() {
    var selection = document.getSelection();

    if (selection.rangeCount > 0) {
      var selectionString = selection.toString(),
        regexChoices = /[(]*([a-zA-Z])[.)][.)]*\s*(.*)/g,
        choicesCleaned = '',
        parentId = getSelectionParentElementId(),
        match;

      while ((match = regexChoices.exec(selectionString)) !== null) {
        if (match.index === regexChoices.lastIndex) {
            regexChoices.lastIndex++;
        }

        choicesCleaned += match[1].toUpperCase() + '. ' + s.capitalize(match[2]) + '\n';
      }

      if ($.trim(selectionString) !== '') {
        switch (parentId) {
          case 'window-oreo':
            var result = $('#window-oreo')[0].innerText.replace(selectionString, choicesCleaned);

            Narek.plainTextOreo.set(result);

            if (!Session.equals('oreoLocked', true)) {
              Narek.plainTextKitkat.set(result);
            }
          break;

          case 'window-kitkat':
            var result = $('#window-kitkat')[0].innerText.replace(selectionString, choicesCleaned);

            Narek.plainTextKitkat.set(result);

            if (!Session.equals('oreoLocked', true)) {
              Narek.plainTextOreo.set(result);
            }
          break;

          default:
            var sourceContent = document.getElementById(parentId).querySelector('.source-content'),
              result = sourceContent.innerText.replace(selectionString, choicesCleaned),
              dataObject = Narek.Honda.loadedFilesCollection.findOne({windowId: parentId});

            Narek.Honda.upsertHondaSourcePlainText(result, dataObject._id, dataObject);
        }
      }
    }
    else {
      console.log('Text is not selected!');
    }
  },

  scrambleAnswers: function(context) {
      if (context) {
        var sourceId = context.windowId;
      }
      else {
        var sourceId = 'window-kitkat';
      }
console.log('scrambleAnswers');
    var answers = this.getAnswers(sourceId),
      answersSymbol = [],
      answersText = [],
      match,
      shuffledAnswersText;

    if (answers && !$.isEmptyObject(answers)) {
      // Load answer symbol and text in different arrays.
      $.each(answers, function(symbol, text) {
        answersSymbol.push(symbol);
        answersText.push(text);
      });

      shuffledAnswersText = _.shuffle(answersText);

      var choicesWithDescriptionsBeforeShuffle = Narek.Answer.getChoicesWithDescriptions();
      var correctAnswerTextBeforeShuffle = Narek.Answer.getTextCorrectAnswer();
console.log('choicesWithDescriptionsBeforeShuffle', choicesWithDescriptionsBeforeShuffle);
      $.each($('#' + sourceId + ' .answer'), function(index, element) {
        $(this).text(answersSymbol[index] + '. ' + shuffledAnswersText[index]);
      });

      var correctAnswerSymbolAfterShuffle = _.invert(this.getAnswers(sourceId))[correctAnswerTextBeforeShuffle];

      if ($('#'+ sourceId + ' .correct-answer').length !== 0) {
        Narek.Answer.updateCorrectAnswer(correctAnswerSymbolAfterShuffle, choicesWithDescriptionsBeforeShuffle);
      }

      if (sourceId === 'window-kitkat') {
        // Session.set('kitkatChanged', true);
        // Narek.Text.rescanSentences();
      }
      else {
        var dataObject = Narek.Honda.loadedFilesCollection.findOne({windowId: sourceId}),
          result = document.getElementById('window-honda-1-sentences').querySelector('.source-content').innerText;

        Narek.Honda.upsertHondaSourcePlainText(result, dataObject._id, dataObject);
      }
    }
  },

  // Gets answers symbol and its text.
  getAnswers: function(sourceId) {
    var allAnswers = $('#'+ sourceId+ ' .answer'),
      answerRegex = /([A-I])[.] (.*) */, // TODO: multiple lines
      match,
      answers = {};

    if (allAnswers) {
      $.each(allAnswers, function(key, answer) {
        if ((match = answerRegex.exec($(answer).text())) !== null) {
          answers[match[1]] = match[2].trim();
        }
      });
    }

    return answers;
  },

  appendCorrectAnswer: function() {
    var answers = Narek.Answer.getAnswers('window-kitkat');

    if (_.size(answers) !== 0) {
      var correctAnswer = $('#window-kitkat .correct-answer'),
        selectedCorrectAnswerSymbol = $('[name=answer-radios]:checked').val();

      // Checks if correct answer block exists and have not added manually by user.
      if (correctAnswer.length !==  0 && Narek.Kitkat.answerAdded !== true) {
        existingCorrectAnswer = Narek.Answer.getCorrectAnswerFromText(correctAnswer.text());

        if (existingCorrectAnswer && existingCorrectAnswer != selectedCorrectAnswerSymbol) {
          Narek.Answer.updateCorrectAnswer(selectedCorrectAnswerSymbol);

          Narek.Kitkat.answerAdded = false;
          Session.set('kitkatChanged', true);

          // Narek.Text.rescanSentences();
        }
      }
      else {
        // Answer has been added.
        if (Narek.Kitkat.answerAdded === true) {
          Narek.Answer.updateCorrectAnswer(selectedCorrectAnswerSymbol);
        }
        // Correct answer block does not exists.
        else {
          Narek.Answer.appendCorrectAnswerTemplate(selectedCorrectAnswerSymbol);

          Narek.Kitkat.answerAdded = true;
        }
      }
    }
    else {
      alert('No answers found!');
    }
  },

  getCorrectAnswerFromText: function(text) {
    var correctAnswerRegex = /The correct answer is \(([A-I])\) *(?:.*\. .*)*/g,
      match = correctAnswerRegex.exec(text);

    return match[1];
  },

  appendCorrectAnswerTemplate: function(correctAnswerSymbol) {
    var answers = Narek.Answer.getAnswers('window-kitkat'),
      correctAnswerElement = '<span contenteditable="false">The correct answer is (' + correctAnswerSymbol.trim() + ') ' + answers[correctAnswerSymbol].trim() + '. </span><span contenteditable="true" class="answers-edit"></span><br><br>',
      choices = [],
      isIncorrect = (Session.get('answerIsIncorrect') ? 'is incorrect. ' : '');

    $.each(answers, function(symbol, text) {
      if (symbol != correctAnswerSymbol) {
        var choice = '<span contenteditable="false">Choice ' + symbol + ' (' + text.trim() + ') ' + isIncorrect + '</span><span contenteditable="true" class="answers-edit"></span>';

        choices.push('<span class="choice">' + choice + '</span>');
      }
    });

    choices.unshift('<span class="correct-answer">' + correctAnswerElement + '</span>');

    var answerTemplate = choices.join('');

    Narek.Helper.insertHtmlAtCaret('<div class="selected-answer" contenteditable="false">' + answerTemplate + '</div><br>', 'correct-answer');
    // Narek.Helper.insertHtmlAtCaret(answerTemplate, 'correct-answer');
  },

  updateCorrectAnswer: function(correctAnswerSymbol, choicesWithDescriptionsBeforeShuffle) {
    // Gets all kitkat sentences and its span elements.
    var correctAnswer = $('#window-kitkat .correct-answer'),
      answers = Narek.Answer.getAnswers('window-kitkat'),
      correctAnswerSymbolRegex = /The correct answer is \(([A-I])\).*/g,
      match,
      choicesWithDescriptions;

    // If answers has been shuffled.
    if (!choicesWithDescriptionsBeforeShuffle) {
      choicesWithDescriptions = Narek.Answer.getChoicesWithDescriptions();
    }
    else {
      choicesWithDescriptions = choicesWithDescriptionsBeforeShuffle;
    }

    if (!correctAnswerSymbol) {
      // Get correct answer symbol.
      if ((match = correctAnswerSymbolRegex.exec(correctAnswer.text())) !== null) {
        correctAnswerSymbol = match[1];
      }
    }

    if (correctAnswerSymbol) {
      // Answer block has been added
      if (Narek.Kitkat.answerAdded === true) {
        Narek.Answer.updateSelectedAnswerBlock(correctAnswerSymbol);
      }
      else {
        var num = Number($('#window-kitkat .answer').last().attr('num')) + 1,
          choices = [],
          isIncorrect = (Session.get('answerIsIncorrect') ? 'is incorrect. ' : '');

        correctAnswer.html('<span contenteditable="false">The correct answer is (' + correctAnswerSymbol.trim() + ') ' + answers[correctAnswerSymbol].trim() + ((/\.$/.test(answers[correctAnswerSymbol].trim()) === false) ? '.' : '') + ' </span><span contenteditable="true" class="sentence" num="' + num + '">' + ((choicesWithDescriptions[answers[correctAnswerSymbol]]) ? choicesWithDescriptions[answers[correctAnswerSymbol]] : '') + '</span>');

        $.each(answers, function(symbol, text) {
          if (symbol != correctAnswerSymbol) {
            num++;

            var choice = '<span contenteditable="false">Choice ' + symbol + ' (' + text.trim() + ') ' + isIncorrect + '</span>' + ((choicesWithDescriptions[answers[symbol]]) ? ('<span contenteditable="true" class="sentence" num="' + num + '">' + choicesWithDescriptions[answers[symbol]] + '</span>') : '');

            choices.push(choice);
          }
        });

        var spanChoices = $('#window-kitkat .choice');

        $.each(choices, function(key, choice) {
          if (spanChoices[key]) {
            $(spanChoices[key]).html(this);
          }
          else {
            $('#window-kitkat .choice').last().after('<br>' + '<span class="choice inline">' + choice + '</span>');
          }
        });
      }
    }
  },

  // Updates selected answer block added by user.
  updateSelectedAnswerBlock: function(correctAnswerSymbol) {
    var correctAnswerAndChoices = Narek.Answer.getTextCorrectAnswerAndChoices(correctAnswerSymbol);

    if (Narek.Kitkat.answerAdded === true) {
      var spanCorrectAnswer = $('#window-kitkat .correct-answer > span[contenteditable=false]'),
        spanChoices = $('#window-kitkat .choice > span[contenteditable=false]');

      if (spanCorrectAnswer.length == 0) {
        spanCorrectAnswer = $('#window-kitkat .correct-answer');
      }

      if (spanChoices.length == 0) {
        spanChoices = $('#window-kitkat .choice');
      }

      spanCorrectAnswer.text(correctAnswerAndChoices.correctAnswer)

      $.each(spanChoices, function(index, el) {
        $(this).text(correctAnswerAndChoices.choices[index]);
      });
    }
    else {
      $('#window-kitkat .correct-answer').text(correctAnswerAndChoices.correctAnswer)

      $.each($('#window-kitkat .choice'), function(index, el) {
        $(this).text(correctAnswerAndChoices.choices[index]);
      });
    }
  },

  getTextCorrectAnswer: function() {
    var correctAnswer = $('#window-kitkat .correct-answer'),
      correctAnswerDescriptionRegex = /The correct answer is \([A-I]\) *([\w-' ]*). *.*/g,
      textCorrectAnswer;

    if ((match = correctAnswerDescriptionRegex.exec(correctAnswer.text())) !== null) {
      textCorrectAnswer = match[1];
    }

    return textCorrectAnswer;
  },

  getTextCorrectAnswerAndChoices: function(correctAnswerSymbol) {
    var choices = [],
      answers = Narek.Answer.getAnswers('window-kitkat'),
      isIncorrect = (Session.get('answerIsIncorrect') ? 'is incorrect. ' : ''),
      selectedCorrectAnswerSymbol;

      if (correctAnswerSymbol) {
        selectedCorrectAnswerSymbol = correctAnswerSymbol;
      }
      else {
        selectedCorrectAnswerSymbol = $('[name=answer-radios]:checked').val()
      }

    $.each(answers, function(symbol, text) {
      if (symbol != selectedCorrectAnswerSymbol) {
        var choice = 'Choice ' + symbol + ' (' + text.trim() + ') ' + isIncorrect;

        choices.push(choice);
      }
    });

    return {
      correctAnswer: 'The correct answer is (' + selectedCorrectAnswerSymbol.trim() + ') ' + answers[selectedCorrectAnswerSymbol].trim() + '. ',
      choices: choices
    };
  },

  // Gets choices and its descriptions.
  getChoicesWithDescriptions: function () {
    var allChoices = $('#window-kitkat .choice'),
      choicesDescriptionsRegex = /Choice ([A-I]) \(.*\) +(?:is incorrect. )?((?:.*\s*)*)/,
      correctAnswer = $('#window-kitkat .correct-answer'),
      correctAnswerDescriptionRegex = /The correct answer is \(([A-I])\) *[\w-' ]*. *((.*\s*)*)/g,
      match,
      choicesWithDescriptions = {},
      answers = Narek.Answer.getAnswers('window-kitkat');

    if (allChoices) {
      // From correct answer.
      if ((match = correctAnswerDescriptionRegex.exec(correctAnswer.text())) !== null) {
        choicesWithDescriptions[answers[match[1]]] = match[2];
      }
      $.each(allChoices, function(key, choice) {

        // Get correct answer symbol.
        if ((match = choicesDescriptionsRegex.exec($(this).text())) !== null) {
          choicesWithDescriptions[answers[match[1]]] = match[2];
        }
      });
    }

    return choicesWithDescriptions;
  }
};
