/* Narek Autoswap object */
Narek.Autoswap = {
  AutoSwapNewSentences: new Mongo.Collection(null),

  generationTime: new ReactiveVar(0),

  sentenceCount: new ReactiveVar(1),

  generateSentences: function(mode) { // Main AutoSwap function called from page
    Narek.Autoswap.AutoSwapNewSentences.remove({}); // Reset the collection
    Session.set('hasGeneratedNewSentences', false);
    //progressJs().start();

    var swapSettings = {
      originalSentence: {
        txt: Narek.selectedSentenceContent.get(),
        gender: Narek.Autoswap.resolveGender(Narek.selectedSentenceContent.get()),
        swaps: 0,
        formatted: Narek.selectedSentenceContent.get()
      },
      genderToSwapTo: $('#gender-radio-buttons-group .active input').val(),
      genderRegex: /((he|his|him)\/(she|her|hers))/gi
    }

    Narek.Autoswap.AutoSwapNewSentences.insert(swapSettings.originalSentence);

    if (Settings.findOne({setting: 'Use alternative autoswap algorithm?'}).value) {
      Narek.Autoswap.generateSentences2(swapSettings); // Use alternative algorithm
      return;
    }

    Narek.Autoswap.AutoSwapNewSentences.find().forEach(function(sentence){
      Narek.Autoswap.oneWaySwap(sentence);
    });
    progressJs().increase(15);
    Narek.Autoswap.AutoSwapNewSentences.find().forEach(function(sentence){
      Narek.Autoswap.reversibleSwap(sentence);
    });
    progressJs().increase(15);
    Narek.Autoswap.AutoSwapNewSentences.find().forEach(function(sentence){
      Narek.Autoswap.diseaseSwap(sentence);
    });
    progressJs().increase(15);
    Narek.Autoswap.AutoSwapNewSentences.find().forEach(function(sentence){
      Narek.Autoswap.monthsToWeeks(sentence);
    });
    progressJs().increase(15);
    Narek.Autoswap.AutoSwapNewSentences.find().forEach(function(sentence){
      Narek.Autoswap.randomizeOtherNumbers(sentence);
    });
    progressJs().increase(15);
    Narek.Autoswap.AutoSwapNewSentences.find().forEach(function(sentence){
      Narek.Autoswap.randomizeAge(sentence);
    });
    progressJs().set(100);

    Meteor.setTimeout(function() {
      progressJs().end();
      $('#window-autoswap .list-group-item').first().trigger('click');
    }, 200);
  },

  generateSentences2: function(swapSettings) {
    //console.log('Using alternative autoswap algorithm!');
    var start = new Date().getTime();
    var sentence = swapSettings.originalSentence;
    var txt = swapSettings.originalSentence.txt;
    var words = txt.match(/\b([^ ]+)\b/g); // Create an array of words in sentence
    var wordCount = words.length;
    var phrases = [];
    var totalNewSentences = 0;

    for (var i = 0; i < wordCount; i++) {
      // Loop once starting at each word
      phrases.push(words[i]);
      var phrase = words[i];

      for (var j = i + 1; j < wordCount; j++) {
        phrase += ' ' + words[j];
        phrases.push(phrase);
      }
    }

    // Start generating new sentences
    var newSentences = [sentence];
    var newSentenceTexts = [sentence.txt];
    var gender = sentence.gender;
    var swapGender = swapSettings.genderToSwapTo;
    var genderRegex = swapSettings.genderRegex;

    var phraseCount = phrases.length;
    var progressIncrement = 85 / phraseCount;
    //console.log('Made ' + phraseCount + ' phrases, progressIncrement: ' + progressIncrement);

    for (var c = 0; c < phraseCount; c++) {
      var v = phrases[c];
      var position = txt.search(v); // position of where phrase starts in sentence (starting with 0)

      var whereFunction = function() {
        var array = this.terms;
        array.pop();
        return array.some(function(el) { return el == v });
      };

      AutoSwapOneWay.find({
        "terms": {
          $regex: new RegExp(v, "i")
        },
        "$where": whereFunction
      }).forEach(function(obj) {
        var terms = obj.terms;
        var termCount = terms.length;
        var foundTermIndex = terms.indexOf(v);
        var newSentencesCount = newSentences.length;

        for (var k = foundTermIndex; k < termCount - 1; k++) {
          for (var n = 0; n < newSentencesCount; n++) {
            var sentence = newSentences[n];
            var term = terms[k].replace(genderRegex, gender === 'male' ? '$2' : '$3');
            var regex = new RegExp('\\b(' + term + ')\\b', 'gi');
            var txt = sentence.txt
              .replace(regex,terms[k+1])
              .replace(genderRegex, swapGender === 'male' ? '$2' : '$3')
              .capitalizeFirstLetter(); // Replace found term and capitalize first letter of sentence
            if (newSentenceTexts.indexOf(txt) > -1) {
              continue;
            }
            var newSentence = {
              txt: txt,
              gender: sentence.gender,
              swaps: sentence.swaps + 1,
              formatted: Narek.Autoswap.highlightSwaps(txt)
            }
            Narek.Autoswap.AutoSwapNewSentences.insert(newSentence);
            newSentences.push(newSentence);
            newSentenceTexts.push(txt);
            Session.set('hasGeneratedNewSentences', true);
            totalNewSentences++;
          }
        }
      });

      AutoSwapReversible.find({
        "terms": {
          $regex: new RegExp(v, "i")
        }
      }).forEach(function(obj) {
        var terms = obj.terms;
        var termCount = terms.length;
        var newSentencesCount = newSentences.length;

        for (var i = 0; i < termCount; i++) { // Any item can be swapped with any other
          var term = terms[i].replace(genderRegex, gender === 'male' ? '$2' : '$3');
          var regex = new RegExp('\\b(' + term + ')\\b', 'gi');
          if (newSentences[0].txt.match(regex)) {
            for (var j = 0; j < termCount; j++) {
              if (j !== i) { // Don't swap it with itself
                for (var n = 0; n < newSentencesCount; n++) {
                  var sentence = newSentences[n];
                  if (sentence.txt.match(terms[j])) {
                    continue;
                  }
                  var txt = sentence.txt
                    .replace(regex,terms[j])
                    .replace(genderRegex, swapGender === 'male' ? '$2' : '$3')
                    .capitalizeFirstLetter();
                  if (newSentenceTexts.indexOf(txt) > -1) {
                    continue;
                  }
                  var newSentence = {
                    txt: txt,
                    gender: sentence.gender,
                    swaps: sentence.swaps + 1,
                    formatted: Narek.Autoswap.highlightSwaps(txt)
                  };
                  Narek.Autoswap.AutoSwapNewSentences.insert(newSentence);
                  newSentences.push(newSentence);
                  newSentenceTexts.push(txt);
                  Session.set('hasGeneratedNewSentences', true);
                  totalNewSentences++;
                }
              }
            }
          }
        }
      });

      // Then, look in diseases dictionary
      DiseaseSynonyms.find({
        "synonyms": {
          $regex: new RegExp(v, "i")
        }
      }).forEach(function(obj) {
        var terms = obj.synonyms;
        var termCount = terms.length;
        var newSentencesCount = newSentences.length;

        for (var i = 0; i < termCount; i++) { // Any item can be swapped with any other
          var regex = new RegExp('\\b(' + terms[i] + ')\\b', 'gi');
          if (newSentences[0].txt.match(regex)) {
            for (var n = 0; n < newSentencesCount; n++) {
              var sentence = newSentences[n];
              for (var j = 0; j < termCount; j++) {
                if (j !== i) { // Don't swap it with itself
                  var txt = sentence.txt.replace(regex,terms[j]);
                  if (newSentenceTexts.indexOf(txt) > -1) {
                    continue;
                  }
                  var newSentence = {
                    txt: txt,
                    gender: sentence.gender,
                    swaps: sentence.swaps + 1,
                    formatted: Narek.Autoswap.highlightSwaps(txt)
                  };
                  Narek.Autoswap.AutoSwapNewSentences.insert(newSentence);
                  newSentences.push(newSentence);
                  newSentenceTexts.push(txt);
                  Session.set('hasGeneratedNewSentences', true);
                  totalNewSentences++;
                }
              }
            }
          }
        }
      });

      var count = c + 1;
    //console.log('Done with phrase ' + count + '/' + phraseCount + ': ' + v);
    //console.log('Total new sentences: ' + totalNewSentences);
      progressJs().increase(progressIncrement);
    }
    progressJs().set(85);

    // Generate and add sentences with x months changed to x weeks
    Narek.Autoswap.AutoSwapNewSentences.find().forEach(function(sentence){
      Narek.Autoswap.monthsToWeeks(sentence);
    });
    progressJs().increase(5);

    // Generate and add sentences with other numbers randomized
    Narek.Autoswap.AutoSwapNewSentences.find().forEach(function(sentence){
      Narek.Autoswap.randomizeOtherNumbers(sentence);
    });
    progressJs().increase(5);

    // Generate and add sentences with randomized age
    Narek.Autoswap.AutoSwapNewSentences.find().forEach(function(sentence){
      Narek.Autoswap.randomizeAge(sentence);
    });
    progressJs().set(100);

    var end = new Date().getTime();
    var time = (end - start) / 1000;

    Narek.Autoswap.generationTime.set(time);
    Narek.Autoswap.sentenceCount.set(Narek.Autoswap.AutoSwapNewSentences.find().count());

    Meteor.setTimeout(function() {
      $('#window-autoswap .list-group-item').first().trigger('click');
      progressJs().end();
    }, 200);
  },

  selectionAutoSwap: function() {
    var start = new Date().getTime();
    var selection = rangy.getSelection();
    var $selection = $(selection.toHtml()); // TODO: Fix
    Session.set('oreoLocked', true);

    if (selection.rangeCount <= 0) {
      alert('Text is not selected!');
      return;
    }

    $selection.each(function( index ) {
      var num = $(this).attr('num');
      var el = $('#window-kitkat span.sentence[num=' + num + ']')[0];
      if (!el) {
        return true;
      }
      var sentenceText = el.innerText;

      Narek.originalSentenceContent.set(sentenceText);
      console.log('originalSentenceContent', sentenceText);
      Narek.selectedSentenceContent.set(sentenceText);
      Narek.selectedSentenceId.set(num);

      Narek.Autoswap.AutoSwapNewSentences.remove({}); // Reset the collection

      var sentence = {
        txt: sentenceText,
        gender: Narek.Autoswap.resolveGender(sentenceText),
        swaps: 0,
        formatted: sentenceText
      };

      var swapSettings = {
        genderToSwapTo: $('#gender-radio-buttons-group .active input').val(),
        genderRegex: /((he|his|him)\/(she|her|hers))/gi
      };

      var txt = sentence.txt;
      var words = txt.match(/\b([^ ]+)\b/g); // Create an array of words in sentence
      var wordCount = words.length;
      var phrases = [];
      var totalNewSentences = 0;

      for (var i = 0; i < wordCount; i++) {
        // Loop once starting at each word
        phrases.push(words[i]);
        var phrase = words[i];

        for (var j = i + 1; j < wordCount; j++) {
          phrase += ' ' + words[j];
          phrases.push(phrase);
        }
      }

      // Start generating new sentences
      var newSentences = [sentence];
      var newSentenceTexts = [sentence.txt];
      var gender = sentence.gender;
      var swapGender = swapSettings.genderToSwapTo;
      var genderRegex = swapSettings.genderRegex;
      var phraseCount = phrases.length;

      for (var c = 0; c < phraseCount; c++) {
        var v = phrases[c];
        var position = txt.search(v); // position of where phrase starts in sentence (starting with 0)

        var whereFunction = function() {
          var array = this.terms;
          array.pop();
          return array.some(function(el) { return el == v });
        };

        AutoSwapOneWay.find({
          "terms": {
            $regex: new RegExp(v, "i")
          },
          "$where": whereFunction
        }).forEach(function(obj) {
          var terms = obj.terms;
          var termCount = terms.length;
          var foundTermIndex = terms.indexOf(v);
          var newSentencesCount = newSentences.length;

          for (var k = foundTermIndex; k < termCount - 1; k++) {
            for (var n = 0; n < newSentencesCount; n++) {
              var sentence = newSentences[n];
              var term = terms[k].replace(genderRegex, gender === 'male' ? '$2' : '$3');
              var regex = new RegExp('\\b(' + term + ')\\b', 'gi');
              var txt = sentence.txt
                .replace(regex,terms[k+1])
                .replace(genderRegex, swapGender === 'male' ? '$2' : '$3')
                .capitalizeFirstLetter(); // Replace found term and capitalize first letter of sentence
              if (newSentenceTexts.indexOf(txt) > -1) {
                continue;
              }
              var newSentence = {
                txt: txt,
                gender: sentence.gender,
                swaps: sentence.swaps + 1,
                formatted: Narek.Autoswap.highlightSwaps(txt)
              }
              Narek.Autoswap.AutoSwapNewSentences.insert(newSentence);
              newSentences.push(newSentence);
              newSentenceTexts.push(txt);
              Session.set('hasGeneratedNewSentences', true);
              totalNewSentences++;
            }
          }
        });

        AutoSwapReversible.find({
          "terms": {
            $regex: new RegExp(v, "i")
          }
        }).forEach(function(obj) {
          var terms = obj.terms;
          var termCount = terms.length;
          var newSentencesCount = newSentences.length;

          for (var i = 0; i < termCount; i++) { // Any item can be swapped with any other
            var term = terms[i].replace(genderRegex, gender === 'male' ? '$2' : '$3');
            var regex = new RegExp('\\b(' + term + ')\\b', 'gi');
            if (newSentences[0].txt.match(regex)) {
              for (var j = 0; j < termCount; j++) {
                if (j !== i) { // Don't swap it with itself
                  for (var n = 0; n < newSentencesCount; n++) {
                    var sentence = newSentences[n];
                    if (sentence.txt.match(terms[j])) {
                      continue;
                    }
                    var txt = sentence.txt
                      .replace(regex,terms[j])
                      .replace(genderRegex, swapGender === 'male' ? '$2' : '$3')
                      .capitalizeFirstLetter();
                    if (newSentenceTexts.indexOf(txt) > -1) {
                      continue;
                    }
                    var newSentence = {
                      txt: txt,
                      gender: sentence.gender,
                      swaps: sentence.swaps + 1,
                      formatted: Narek.Autoswap.highlightSwaps(txt)
                    };
                    Narek.Autoswap.AutoSwapNewSentences.insert(newSentence);
                    newSentences.push(newSentence);
                    newSentenceTexts.push(txt);
                    Session.set('hasGeneratedNewSentences', true);
                    totalNewSentences++;
                  }
                }
              }
            }
          }
        });

        // Then, look in diseases dictionary
        DiseaseSynonyms.find({
          "synonyms": {
            $regex: new RegExp(v, "i")
          }
        }).forEach(function(obj) {
          var terms = obj.synonyms;
          var termCount = terms.length;
          var newSentencesCount = newSentences.length;

          for (var i = 0; i < termCount; i++) { // Any item can be swapped with any other
            var regex = new RegExp('\\b(' + terms[i] + ')\\b', 'gi');
            if (newSentences[0].txt.match(regex)) {
              for (var n = 0; n < newSentencesCount; n++) {
                var sentence = newSentences[n];
                for (var j = 0; j < termCount; j++) {
                  if (j !== i) { // Don't swap it with itself
                    var txt = sentence.txt.replace(regex,terms[j]);
                    if (newSentenceTexts.indexOf(txt) > -1) {
                      continue;
                    }
                    var newSentence = {
                      txt: txt,
                      gender: sentence.gender,
                      swaps: sentence.swaps + 1,
                      formatted: Narek.Autoswap.highlightSwaps(txt)
                    };
                    Narek.Autoswap.AutoSwapNewSentences.insert(newSentence);
                    newSentences.push(newSentence);
                    newSentenceTexts.push(txt);
                    Session.set('hasGeneratedNewSentences', true);
                    totalNewSentences++;
                  }
                }
              }
            }
          }
        });

        var count = c + 1;
      }

      // Generate and add sentences with x months changed to x weeks
      Narek.Autoswap.AutoSwapNewSentences.find().forEach(function(sentence){
        Narek.Autoswap.monthsToWeeks(sentence);
      });

      // Generate and add sentences with other numbers randomized
      Narek.Autoswap.AutoSwapNewSentences.find().forEach(function(sentence){
        Narek.Autoswap.randomizeOtherNumbers(sentence);
      });

      // Generate and add sentences with randomized age
      Narek.Autoswap.AutoSwapNewSentences.find().forEach(function(sentence){
        Narek.Autoswap.randomizeAge(sentence);
      });

      var end = new Date().getTime();
      var time = (end - start) / 1000;

      Narek.Autoswap.generationTime.set(time);
      Narek.Autoswap.sentenceCount.set(Narek.Autoswap.AutoSwapNewSentences.find().count());

      var bestSentence = Narek.Autoswap.AutoSwapNewSentences.findOne({}, {sort: {swaps: -1}}); // Find the one with most swaps

      if (bestSentence) {
        var result = $('#window-kitkat')[0].innerText
          .replace(Narek.originalSentenceContent.get(), ((/\s+$/.test(bestSentence.txt)) ? bestSentence.txt : bestSentence.txt + ' '));

        Narek.plainTextKitkat.set(result);
        Meteor.setTimeout(function(){
          $('#window-kitkat span.sentence[num=' + Narek.selectedSentenceId.get() + ']')[0].innerHTML = bestSentence.formatted + ' ';
        },100);
      }
    });
  },

  bulkAutoSwap: function() {
  },

  bulkFileSwap: function() {
    var len = Narek.oreoFiles.length;

    if (Narek.loadedFileIndex < len) {
      //loadedFileIndex = 0;
      //for (var i = 0; i < len; i++) {
      Narek.File.readFile(Narek.oreoFiles[Narek.loadedFileIndex], Narek.File.loadSelectedFile);
      Meteor.setTimeout(function(){
        var regex = /.+[?]/;
        var questionStemText = Narek.plainTextKitkat.get().match(regex)[0];
        var questionStem = {
          txt: questionStemText,
          gender: Narek.Autoswap.resolveGender(questionStemText),
          swaps: 0
        }
        Narek.Autoswap.AutoSwapNewSentences.remove({}); // Reset the collection
        Narek.Autoswap.AutoSwapNewSentences.insert(questionStem);
        Narek.Autoswap.AutoSwapNewSentences.find().forEach(function(sentence){
          Narek.Autoswap.oneWaySwap(sentence);
        });
        Narek.Autoswap.AutoSwapNewSentences.find().forEach(function(sentence){
          Narek.Autoswap.reversibleSwap(sentence);
        });
        Narek.Autoswap.AutoSwapNewSentences.find().forEach(function(sentence){
          Narek.Autoswap.diseaseSwap(sentence);
        });
        Narek.Autoswap.AutoSwapNewSentences.find().forEach(function(sentence){
          Narek.Autoswap.monthsToWeeks(sentence);
        });
        Narek.Autoswap.AutoSwapNewSentences.find().forEach(function(sentence){
          Narek.Autoswap.randomizeOtherNumbers(sentence);
        });
        Narek.Autoswap.AutoSwapNewSentences.find().forEach(function(sentence){
          Narek.Autoswap.randomizeAge(sentence);
        });
        var newQuestionStem = Narek.Autoswap.AutoSwapNewSentences.findOne({}, {sort: {swaps: -1}}); // Find the one with most swaps
        if (newQuestionStem) {
          var editedText = newQuestionStem.txt;
          var result = Narek.plainTextKitkat.get()
            .replace(questionStemText, ((/\s+$/.test(editedText)) ? editedText : editedText + ' '));
          Narek.plainTextKitkat.set(result);
        }

        //var answerChoices = '';
        saveTextAsFile(Narek.plainTextKitkat.get(), Narek.oreoFiles[Narek.loadedFileIndex].name.replace('OREO', 'HONDA'));
        Narek.loadedFileIndex++;
        if (!Session.get('differenceCheck')) {
          $('#btn-difference-check').trigger('click');
        }
      }, 100);
      //}
      // autoswap and gender swap on questions and answers
      // 3. the ANSWER explanations will have to change dependently based on what the answer choice was AUTOSWAPED to.
      // 4. SCRAMBLE the answer choices, while keeping the answer explanation pairing to the appropriate choice.
    }
  },

  highlightSwaps: function(sentenceText) {
    var diff = JsDiff.diffWords(Narek.selectedSentenceContent.get(), sentenceText);
    var formattedText = '';

    diff.forEach(function(part){
      if (part.added) {
        formattedText += '<span class="autoswap-formatted">' + part.value + '</span>';
      }
      else if (!part.removed) {
        formattedText += part.value;
      }
    });

    return formattedText;
  },

  highlightOriginal: function() {
    var sentence = Narek.Autoswap.AutoSwapNewSentences.findOne({},{
      sort: [["swaps", "desc"]]
    });
    if (sentence) {
      var diff = JsDiff.diffWords(Narek.originalSentenceContent.get(), sentence.txt);
      var formattedText = '';

      diff.forEach(function(part){
        if (part.removed) {
          formattedText += '<span class="autoswap-formatted">' + part.value + '</span>';
        }
        else if (!part.added) {
          formattedText += part.value;
        }
      });

      return formattedText;
    }
    else {
      return Narek.originalSentenceContent.get();
    }
  },

  resolveGender: function(sentenceText) {
    var result = 'unspecified';

    GenderPronouns.find().forEach(function(obj) {
      var matchMale = sentenceText.match(RegExp('(\\b' + obj.malePronoun + '\\b)', 'gi'));
      var matchFemale = sentenceText.match(RegExp('(\\b' + obj.femalePronoun + '\\b)', 'gi'));

      if (matchMale) {
        result = 'male';
        return false;
      }
      else if (matchFemale) {
        result = 'female';
        return false;
      }
    });

    return result;
  },

  switchGender: function(sentence) {
    if (sentence.gender === 'male') {
      GenderPronouns.find().forEach(function(obj) {
        if (sentence.text.contains(obj.malePronoun)) {
          sentence = sentence.replace(obj.malePronoun, obj.femalePronoun);
        }
      });
      return sentence.replace('his', 'her').replace('him', 'her');
    }
    else if (obj.gender === 'female') {
      GenderPronouns.find().forEach(function(obj) {
        if (sentence.contains(obj.femalePronoun)) {
          sentence = sentence.replace(obj.femalePronoun, obj.malePronoun);
        }
      });
      return sentence.replace('her', 'his').replace('hers', 'his');
    }
    return sentence;
  },

  ensureCorrectGender: function(sentence, gender) {
    var sentenceGender = Narek.Autoswap.resolveGender(sentence);
    if (gender === sentenceGender) {
      return sentence;
    }
    else {
      return Narek.Autoswap.switchGender(sentence);
    }
  },

  oneWaySwap: function(sentence) {
    var newSentences = [sentence];
    var gender = sentence.gender;
    var swapGender = $('#gender-radio-buttons-group .active input').val();
    var genderRegex = /((he|his|him)\/(she|her|hers))/gi;

    AutoSwapOneWay.find().forEach(function(obj) {
      var terms = obj.terms;
      var termCount = terms.length;
      var newSentencesCount = newSentences.length;

      for (var k = 0; k < newSentencesCount; k++) {
        for (var i = 0; i < termCount - 1; i++) { // The last item in the array can't be swapped from
          var term = terms[i].replace(genderRegex, gender === 'male' ? '$2' : '$3');
          var regex = new RegExp('\\b(' + term + ')\\b', 'gi');
          if (newSentences[k].txt.match(regex)) {
            for (var j = i + 1; j < termCount; j++) { // Start with next item in array
              var txt = newSentences[k].txt
                  .replace(regex,terms[j])
                  .replace(genderRegex, swapGender === 'male' ? '$2' : '$3')
                  .capitalizeFirstLetter(); // Replace found term and capitalize first letter of sentence
              var newSentence = {
                txt: txt,
                gender: newSentences[k].gender,
                swaps: newSentences[k].swaps + 1,
                formatted: Narek.Autoswap.highlightSwaps(txt)
              };

              Narek.Autoswap.AutoSwapNewSentences.insert(newSentence);
              newSentences.push(newSentence);
              Session.set('hasGeneratedNewSentences', true);
            }
          }
        }
      }
    });
  },

  reversibleSwap: function(sentence) {
    var newSentences = [sentence];
    var gender = sentence.gender;
    var swapGender = $('#gender-radio-buttons-group .active input').val();
    var genderRegex = /((he|his|him)\/(she|her|hers))/gi;

    AutoSwapReversible.find().forEach(function(obj) {
      var terms = obj.terms;
      var termCount = terms.length;
      var newSentencesCount = newSentences.length;

      for (var k = 0; k < newSentencesCount; k++) {
        for (var i = 0; i < termCount; i++) { // Any item can be swapped with any other
          var term = terms[i].replace(genderRegex, gender === 'male' ? '$2' : '$3');
          var regex = new RegExp('\\b(' + term + ')\\b', 'gi');
          if (newSentences[k].txt.match(regex)) {
            for (var j = 0; j < termCount; j++) {
              if (j !== i) { // Don't swap it with itself
                var txt = newSentences[k].txt
                    .replace(regex,terms[j])
                    .replace(genderRegex, swapGender === 'male' ? '$2' : '$3')
                    .capitalizeFirstLetter();
                var newSentence = {
                  txt: txt,
                  gender: newSentences[k].gender,
                  swaps: newSentences[k].swaps + 1,
                  formatted: Narek.Autoswap.highlightSwaps(txt)
                };
                Narek.Autoswap.AutoSwapNewSentences.insert(newSentence);
                newSentences.push(newSentence);
                Session.set('hasGeneratedNewSentences', true);
              }
            }
          }
        }
      }
    });
  },

  diseaseSwap: function(sentence) {
    var newSentences = [sentence];
    DiseaseSynonyms.find().forEach(function(obj) {
      var terms = obj.synonyms;
      var termCount = terms.length;
      var newSentencesCount = newSentences.length;
      for (var k = 0; k < newSentencesCount; k++) {
        for (var i = 0; i < termCount; i++) { // Any item can be swapped with any other
          var regex = new RegExp('\\b(' + terms[i] + ')\\b', 'gi');
          if (newSentences[k].txt.match(regex)) {
            for (var j = 0; j < termCount; j++) {
              if (j !== i) { // Don't swap it with itself
                var txt = newSentences[k].txt.replace(regex,terms[j]);
                var newSentence = {
                  txt: txt,
                  gender: newSentences[k].gender,
                  swaps: newSentences[k].swaps + 1,
                  formatted: Narek.Autoswap.highlightSwaps(txt)
                };
                Narek.Autoswap.AutoSwapNewSentences.insert(newSentence);
                newSentences.push(newSentence);
                Session.set('hasGeneratedNewSentences', true);
              }
            }
          }
        }
      }
    });
  },

  randomizeAge: function(sentence) {
    var ageSuffixes = ['-yr-old', '-year-old', ' year old'].join('|');
    var regex = new RegExp('(\\d{1,3})(' + ageSuffixes + ')', 'gi');
    var tempResult = regex.exec(sentence.txt);
    if (tempResult) {
      var newAge = randomizeNumber(parseInt(tempResult[1])); // [1] contains the number
      var txt = sentence.txt.replace(regex, newAge + '$2');
      var newSentence = {
        txt: txt,
        gender: sentence.gender,
        swaps: sentence.swaps + 1,
        formatted: Narek.Autoswap.highlightSwaps(txt)
      };
      Narek.Autoswap.AutoSwapNewSentences.insert(newSentence);
      Session.set('hasGeneratedNewSentences', true);
    }
  },

  monthsToWeeks: function(sentence) {
    var monthsSuffixes = ['month', 'months'].join('|');
    var regex = new RegExp('(\\d{1,2}) (' + monthsSuffixes + ')\\b', 'gi');
    var tempResult = regex.exec(sentence.txt);
    if (tempResult) {
      var max, min;
      var months = parseInt(tempResult[1]); // [1] contains the number
      if (months === 1) {
        min = 3;
        max = 4;
      }
      else if (months === 2) {
        min = 5;
        max = 8;
      }
      else {
        min = 3 * months;
        max = 4 * months;
      }
      var weeks = Math.floor(Math.random() * (max - min + 1)) + min;
      var txt = sentence.txt.replace(regex, weeks + ' weeks');
      var newSentence = {
        txt: txt,
        gender: sentence.gender,
        swaps: sentence.swaps + 1,
        formatted: Narek.Autoswap.highlightSwaps(txt)
      };
      Narek.Autoswap.AutoSwapNewSentences.insert(newSentence);
      Session.set('hasGeneratedNewSentences', true);
    }
  },

  randomizeOtherNumbers: function(sentence) {
    var ageSuffixes = ['-yr-old', '-year-old', ' year old'].join('|');
    var monthsSuffixes = ['month'].join('|'); // Weeks can be randomized
    var regex = new RegExp('\\b(\\d+)(?!\\d*\\s*(?:' + ageSuffixes + '|' + monthsSuffixes + '))', 'gi');
    var tempResult = regex.exec(sentence.txt);
    if (tempResult) {
      var txt = sentence.txt.replace(regex, function(match){
        number = Number(match);
        if (number >= 10) {
          return randomizeNumber(number);
        }
        var min = number - 1;
        if (min < 1) {
          min = 1;
        }
        var max = number + 1;
        do {
          number = Math.floor(Math.random() * (max - min + 1)) + min;
        } while(number == match);
        return number;
      });
      var newSentence = {
        txt: txt,
        gender: sentence.gender,
        swaps: sentence.swaps + 1,
        formatted: Narek.Autoswap.highlightSwaps(txt)
      };
      Narek.Autoswap.AutoSwapNewSentences.insert(newSentence);
      Session.set('hasGeneratedNewSentences', true);
    }
  },

  editSwap: function(type) { // type == 'comma' || 'and'
    var selection = rangy.getSelection();

    if (selection.rangeCount <= 0) {
      alert('Text is not selected!');
      return;
    }
    if (type === 'and' && !selection.toString().match(/\band\b/)) {
      alert('No "and" found in selection!');
      return;
    }
    if (type === 'comma' && !selection.toString().match(/,/)) {
      alert('No comma found in selection!');
      return;
    }

    var selectionString = selection.toString();
    var range = selection.getRangeAt(0);

    if (selectionString[0] === ' ') {
      range.moveStart(1); // Move selection 1 character to the right
      selection.setSingleRange(range);
      selectionString = selection.toString();
    }

    var l = selectionString.slice(-1); // Last character
    while (l.match(/[.!?:\s]/)) {
      range.moveEnd(-1); // Trim last character off selection
      selection.setSingleRange(range);
      selectionString = selection.toString();
      l = selectionString.slice(-1);
    }

    var selectionStart = range.startOffset;
    var selectionEnd = range.endOffset;
    var m;
    var parts = [];
    var re = /\b((?:(?!and)(?:[\w-°/]+?) *)+(?!and))/gi; // TODO: Fix to ignore 'and' without comma (for commaswap)

    while ((m = re.exec(selectionString)) !== null) {
      if (m.index === re.lastIndex) {
        re.lastIndex++;
      }
      parts.push(m[0]);
    }

    var shuffled = _.shuffle(parts);
    var newString;

    if (type === 'and') {
      newString = parts.shuffle().join(' and ');

      while (selectionString === newString) {
        newString = parts.shuffle().join(' and ');
      }
    }

    else if (type == 'comma') {
      var len = shuffled.length;
      newString = shuffled[0] + ', ';

      for (var i = 1; i < len; i++) {
        if (i === (len - 1)) {
          if (selectionString.match(/\band\b/)) {
            newString += ' and ';
          }
          else if (selectionString.match(/\bor\b/)) {
            newString += ' or ';
          }
          newString += shuffled[i];
        }
        else {
          newString += shuffled[i] + ', ';
        }
      }
    }

    $('#window-edit')[0].innerText
      .replace(selectionString, newString)
      .capitalizeFirstLetter();

    Narek.selectedSentenceContent.set($('#window-edit')[0].innerText
      .replace(selectionString, newString)
      .capitalizeFirstLetter());

    Meteor.setTimeout(function() {
      var el = document.getElementById('window-edit').childNodes[0];
      var range = rangy.createRange();
      range.selectNodeContents(el);
      range.setStart(el, selectionStart);
      range.setEnd(el, selectionEnd);
      var sel = rangy.getSelection();
      sel.setSingleRange(range);
    }, 100);
  }
};
