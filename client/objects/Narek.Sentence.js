/* Narek Sentence object */
Narek.Sentence = {
  verify: function(sentences) {
    var result = [];

    if (sentences !== null) {
      if (Array.isArray(sentences)) {
        sentences.forEach(function (sentence) {
          sentence = sentence.trim();

          // Checks if sentence does not end with whitespace then add one.
          if (/\s+$/.test(sentence) === false) {
            sentence += ' ';
          }

          result.push(sentence);
        });
      }
      else {
        var sentence = sentences.trim();

        // Checks if sentence does not end with whitespace then add one.
        if (/\s+$/.test(sentence) === false) {
          sentence += ' ';
        }

        result = sentence;
      }
    }

    return result;
  },

  resetSelectedSentenceVariables: function() {
    Narek.selectedSentenceId.set(null);
    Narek.selectedSentenceContent.set(null);
    Narek.selectedSentenceSource.set(null);

    $('.sentence-highlighted').removeClass('sentence-highlighted');
  },

  changeSelectedSentence: function(sameSentence) {
    if (Session.equals('sentenceMode', true)) {
      var source = Narek.selectedSentenceSource.get(),
        editedText = $('#window-edit')[0].innerText;

      if (Session.get('displayVitals')) {
        $('#window-edit').empty();
      }

      switch (source) {
        case 'oreo':
          var result = $('#window-oreo')[0].innerText.replace(Narek.originalSentenceContent.get(), ((/\s+$/.test(editedText)) ? editedText : editedText + ' '));

          Narek.plainTextOreo.set(result);

          if (!Session.equals('oreoLocked', true)) {
            Narek.plainTextKitkat.set(result);
          }

          Narek.Text.rescanSentences();
        break;

        case 'kitkat':
          var result = $('#window-kitkat')[0].innerText.replace(Narek.originalSentenceContent.get(), ((/\s+$/.test(editedText)) ? editedText : editedText + ' '));

          Narek.plainTextKitkat.set(result);

          if (!Session.equals('oreoLocked', true)) {
            Narek.plainTextOreo.set(result);
          }

          Narek.Text.rescanSentences();
        break;
      }

      if (Session.get('dragAndDropActive') === true) {
        Session.set('dragAndDropActive', false);
      }

      var editedSentenceCount = Narek.Text.getWholeSentences(editedText).length;

      if (sameSentence) {
        Narek.Sentence.selectSameSentence();
      }
      else if (editedSentenceCount > 1) {
        Narek.Sentence.selectNextSentence(editedSentenceCount);
      }
      else {
        Narek.Sentence.selectNextSentence(1);
      }

      Session.set('selectedSentenceEdited', false);
    }
  },

  /* Sets sentence variables to the sentence with same ID. */
  selectSameSentence: function() {
    var selectedSentenceId = Narek.selectedSentenceId.get();

    if (selectedSentenceId) {
      var sentenceId = Number(selectedSentenceId) + 1,
        source = Narek.selectedSentenceSource.get();

      switch (source) {
        case 'oreo':
          var $sameSentence = $('#window-oreo span[num=' + sentenceId  + ']');
        break;

        case 'kitkat':
          var $sameSentence = $('#window-kitkat span[num=' + sentenceId  + ']');
        break;
      }

      if ($sameSentence[0] && $sameSentence[0].innerText) {
        var selectedSentenceContent = $sameSentence[0].innerText;

        $('#window-edit').empty();

        Narek.selectedSentenceContent.set(selectedSentenceContent);
        Narek.originalSentenceContent.set(selectedSentenceContent);

        selectText('window-edit');
      }
      else {
        Narek.Sentence.resetSelectedSentenceVariables();
      }
    }
  },

  /* Sets next sentence variables */
  selectNextSentence: function(count) {
    var selectedSentenceId = Narek.selectedSentenceId.get();

    if (selectedSentenceId) {
      if (!count) {
        count = 1;
      }
      var nextSentenceId = Number(selectedSentenceId) + count,
        source = Narek.selectedSentenceSource.get();

      switch (source) {
        case 'oreo':
          var $nextSentence = $('#window-oreo span[num=' + ((count > 1) ? nextSentenceId - 1 : nextSentenceId)  + ']');
        break;

        case 'kitkat':
          var $nextSentence = $('#window-kitkat span[num=' + ((count > 1) ? nextSentenceId - 1 : nextSentenceId)  + ']');
        break;
      }

      if ($nextSentence[0] && $nextSentence[0].innerText) {
        var nextSentenceContent = $nextSentence[0].innerText;

        $('#window-edit').empty();

        Narek.selectedSentenceId.set(nextSentenceId);
        Narek.selectedSentenceContent.set(nextSentenceContent);
        Narek.originalSentenceContent.set(nextSentenceContent);

        selectText('window-edit');
      }
      else {
        Narek.Sentence.resetSelectedSentenceVariables();
      }
    }
  },

  /* Sets previous sentence variables */
  selectPreviousSentence: function() {
    if (Narek.selectedSentenceId.get() !== null) {
      var previousSentenceId = Number(Narek.selectedSentenceId.get()) - 1,
        source = Narek.selectedSentenceSource.get();

        switch (source) {
          case 'oreo':
            var $previousSentence = $('#window-oreo span[num=' + previousSentenceId  + ']');
          break;

          case 'kitkat':
            $previousSentence = $('#window-kitkat span[num=' + previousSentenceId  + ']');
          break;
        }

      if ($previousSentence[0] && $previousSentence[0].innerText) {
        var previousSentenceContent = $previousSentence[0].innerText;

        Narek.selectedSentenceId.set(previousSentenceId);
        Narek.selectedSentenceContent.set(previousSentenceContent);
        Narek.originalSentenceContent.set(previousSentenceContent);

        selectText('window-edit');
      }
      else {
        Narek.Sentence.resetSelectedSentenceVariables();
      }
    }
  }
};
