/* Narek Honda object */
Narek.Honda = {
  loadedFilesCollection: new Mongo.Collection(null),
  buildFile: new ReactiveVar(''),
  buildFileSentences: new Mongo.Collection(null),
  displayAutoSwap: new ReactiveVar(false), // unused
  buildFileChanged: new ReactiveVar(false),
  buildSenteceRemoved: new ReactiveVar(false),
  /* Options for drag & drop sentences */

  /* Loads Honda files */
  loadFiles: function(files, callback) {
    // Clean loaded files collection */
    Narek.Honda.loadedFilesCollection.remove({});
    Session.set('hondaContentChanged', false);

    var fileCount = files.length,
      filesRead = 0;

    $.each(files, function(index, file) {
      var collectionName = 'window-honda-' + (index + 1) + '-sentences';

      Narek.File.readFile(file, function(result) {
        var sentences = Narek.Text.readSentencesForHonda(result),
          lines = readLines(result, 'honda'),
          // lines = readLines(result),
          brokenSentences = Narek.Honda.autoBreakSentences(readLines(result, 'autobreak')),
          fileName = file.name.replace(/\.[a-z0-9]+$/i, '');

        Narek.Honda.History.loadedFilesWindowIdsAndContent.push({id: collectionName, content: result, name: fileName});

        Narek.Honda.loadedFilesCollection.insert({
          windowId: collectionName,
          name: fileName,
          plainText: result,
          plainSentences: lines,
          brokenSentences: brokenSentences,
          order: index + 1
        });

        Session.set(collectionName + '-checked', false);
        filesRead++;

        if (filesRead === fileCount) {
          if (callback) {
            callback();
          }
        }
      });
    });
  },

  upsertHondaSourcePlainText: function(text, id, object) {
    var sortableSentences = Narek.Text.readSentencesForHonda(text),
      lines = readLines(text, 'honda'),
      // lines = readLines(text),
      brokenSentences = Narek.Honda.autoBreakSentences(readLines(text));
      plainSentences = lines,
      brokenSentences = brokenSentences;

    var updates = {plainText: text, plainSentences: plainSentences, brokenSentences: brokenSentences};

    if (id === null) {
      updates.windowId = object.windowId;
      updates.name = object.name;
    }

    if (object) {}

    Narek.Honda.loadedFilesCollection.upsert(
      {_id: id},
      {$set:
        updates
      }
    );
  },

  rescanSourceText: function(windowIds) {
    if (windowIds) {
      windowIds.forEach(function(windowId) {
        if (windowId === 'window-honda-build') {

        }
        else {
          var windowEl = document.getElementById(windowId),
            text = windowEl.querySelector('.source-content').innerText,
            dataObject = Blaze.getData(windowEl);

          Narek.Honda.upsertHondaSourcePlainText(text, dataObject._id, dataObject);
        }
      });
    }
  },

  updateLoadedFilesOrder: function() {
    var order = 1;

    Narek.Honda.loadedFilesCollection.find({}, {fields: {_id: true}}).fetch().forEach(function(loadedFile) {
      Narek.Honda.loadedFilesCollection.update(
        {_id: loadedFile._id},
        {$set:
          {order: order}
        }
      );

      order++;
    });
  },

  updateBuildFileOrder: function() {
    var order = 1;

    Narek.Honda.buildFileSentences.find({}, {sort: {order: 1}}).fetch().forEach(function(buildSentence) {
      Narek.Honda.buildFileSentences.update(
        {_id: buildSentence._id},
        {$set:
          {order: order}
        }
      );

      order++;
    });
  },

  updateFileName: function(name, id) {
    Narek.Honda.loadedFilesCollection.update(
      {_id: id},
      {$set:
        {name: name}
      }
    );
  },

  addSentenceToBuildFile: function(sentenceText, order) {
    var highestOrder = Narek.Honda.buildFileSentences.findOne({}, {sort: {order: -1}}),
      newOrder;

    if (order !== undefined) {
      newOrder = order;
    }
    else {
      if (highestOrder) {
        newOrder = highestOrder.order + 1;
      }
      else {
        newOrder = 1;
      }
    }

    Narek.Honda.buildFileSentences.insert({text: sentenceText, order: newOrder});
  },

  // Auto breaks the given lines into sentences.
  autoBreakSentences: function(lines) {
    var sentences = Narek.Honda.getSentencesFromLines(lines),
      result = '';

    sentences = Narek.Honda.findAndMergeAnswers(sentences);

    var result = Narek.Honda.convertToStringArray(sentences);

    return result.join('<br><span class="empty-line"></span><br>');
  },

  convertToStringArray: function(htmlObjectArray) {
    var result = [];

    htmlObjectArray.forEach(function(object) {
      result.push($('<div/>').append(object).html());
    });

    return result;
  },

  getSentencesFromLines: function(lines) {
    var sentences = [];

    _.compact(lines).forEach(function(line, index, source) {
      var htmlSentence = $.parseHTML(line);

      if (htmlSentence.length > 1) {
        htmlSentence.forEach(function(sentence) {
          sentences.push(sentence);
        });
      }
      else {
        sentences.push(htmlSentence[0]);
      }
    });

    return sentences;
  },

  // Returns array of html ebject strings.
  findAndMergeAnswers: function(sentences) {
    var answersIndex = [],
      answers = [];

    sentences.forEach(function(sentence, index) {
      var $sentence = $(sentence);

        if ($sentence.hasClass('answer')) {
          answersIndex.push(index);
          answers.push($sentence.text());
          num = $sentence.attr('num');
        }
    });

    if (answersIndex.length !== 0) {
      sentences.splice(answersIndex[0], answersIndex.length, $.parseHTML('<span class="sentence" num="' + num + '">' + answers.join('<br>') + '</span>')[0]);
    }

    return sentences;
  },

  initHondaDragAndDrop: function() {
    if ($('.source-content').sortable('instance')) {
      $('.source-content').sortable('enable');
    }
    else {
      $('.source-content').sortable({
        connectWith: ['.source-content', '#window-honda-build'],
        cursor: 'move',
        dropOnEmpty: true,
        helper: function(e, item) {
          tempHelper = item.clone();
          tempHelper.addClass('temporary-drag-element');

          tempHelper.insertAfter(item);

          return item.clone();
        },
        placeholder: {
          element: function(e, ui) {
              return $('<div style="border: 1px solid red"><br>' + e[0].innerHTML + '<br><br></div>');
          },
          update: function() {
              return;
          }
        },
        start: function(event, ui) {
          Session.set('sourceOrderChanged', false);
        },
        stop: function(event, ui) {
          if (Session.equals('sourceOrderChanged', false)) {
            var $item = $(ui.item);

            tempHelper && tempHelper.remove();

            $item.prepend('<br><br>');
            $item.append('<br><br>');

            Narek.Honda.rescanSourceText([this.parentElement.id]);
            Narek.Honda.History.saveState('hondaDragAndDropStop');
          }
        },
        change: function(event, ui) {
          Session.set('sourceOrderChanged', true);
        },
        update: function(event, ui) {
          tempHelper && tempHelper.remove();

          var $item = $(ui.item);

          if (this.parentElement.id !== ui.item[0].parentElement.parentElement.id) {
            var rescanWindowIds = [];

            rescanWindowIds.push(this.parentElement.id);

            if (ui.item[0].parentElement.parentElement.id !== '') {
              rescanWindowIds.push(ui.item[0].parentElement.parentElement.id);
            }

            Session.set('rescanWindowIds', rescanWindowIds);
          }

          if (this.parentElement.id === ui.item[0].parentElement.parentElement.id) {
            var rescanWindowIds = Session.get('rescanWindowIds');

            if (!rescanWindowIds) {
              rescanWindowIds = [this.parentElement.id];
            }

            $item.prepend('<br><br>');
            $item.append('<br><br>');

            Narek.Honda.rescanSourceText(rescanWindowIds);

            if (Narek.Honda.buildSenteceRemoved.get() === true) {
              $item.remove();
              var itemRemoved = Blaze.getData(ui.item[0]);

              Narek.Honda.buildFileSentences.remove(itemRemoved._id);

              Narek.Honda.buildSenteceRemoved.set(false);
            }

            Narek.Honda.History.saveState('hondaDragAndDropUpdate');
          }
        }
      });
    }

    if ($('#window-honda-build').sortable('instance')) {
      $('#window-honda-build').sortable('enable');
    }
    else {
      $('#window-honda-build').sortable({
        connectWith: ['.source-content'],
        cursor: 'move',
        dropOnEmpty: true,
        helper: 'clone',
        placeholder: {
          element: function(e, ui) {
              return $('<div style="border: 1px solid red"><br>' + e[0].innerHTML + '</div>');
          },
          update: function() {
              return;
          }
        },
        stop: function(event, ui) {
          Narek.Honda.updateSortableOrder(ui, Narek.Honda.buildFileSentences);
          Narek.Honda.History.saveState('hondaBuildFileUpdateSortableOrder');
        },
        remove: function(event, ui) {
          // if (Session.equals('honda-cut-copy-mode', true)) {
            Narek.Honda.buildSenteceRemoved.set(true);
          // }
        },
        receive: function(event, ui) {
          var rescanWindowIds = Session.get('rescanWindowIds'),
            receivedItemIndex = ui.item.index() + 1,
            addBuildSentence = function() {
              Narek.Honda.addSentenceToBuildFile(ui.item[0].innerText);
              Narek.Honda.rescanSourceText(rescanWindowIds);
              Narek.Honda.History.saveState('hondaDragAndDropBuildReceive');
            },
            addBuildSentenceWithOrder = function(order) {
              Narek.Honda.addSentenceToBuildFile(ui.item[0].innerText, order);
              Narek.Honda.updateBuildFileOrder();
              Narek.Honda.rescanSourceText(rescanWindowIds);
              Narek.Honda.History.saveState('hondaDragAndDropBuildReceive');
            };

          if (receivedItemIndex === 1) {
            var existingSentenceWithOrder = Narek.Honda.buildFileSentences.findOne({order: receivedItemIndex + 1});

            if (existingSentenceWithOrder) {
              addBuildSentenceWithOrder(receivedItemIndex);
            }
            else {
              addBuildSentence();
            }
          }
          else {
            var existingSentenceWithOrder = Narek.Honda.buildFileSentences.findOne({order: receivedItemIndex});

            if (existingSentenceWithOrder) {
              var nextSentence = Narek.Honda.buildFileSentences.findOne({order: receivedItemIndex + 1});

              if (nextSentence) {
                var newOrder = (existingSentenceWithOrder.order + 1) / 2;

                addBuildSentenceWithOrder(newOrder);
              }
              else {
                var prevSentence = Narek.Honda.buildFileSentences.findOne({order: receivedItemIndex - 1}),
                  newOrder = (existingSentenceWithOrder.order + prevSentence.order) / 2;

                addBuildSentenceWithOrder(newOrder);
              }
            }
            else {
              addBuildSentence();
            }
          }
        }
      });
    }

    $('.empty-line').addClass('inline-block');
    $('.empty-line').text('\xa0');
  },

  deInitHondaDragAndDrop: function() {
    $('.source-content').sortable('disable');

    $('.empty-line').removeClass('inline-block');
    $('.empty-line').text('');
  },

  updateSortableOrder: function(ui, collection) {
    // get the dragged html element and the one before and one after it
    el = ui.item.get(0);
    before = ui.item.prev().get(0);
    after = ui.item.next().get(0);

    if (Blaze.getData(el)) {
      //  Blaze.getData takes as a parameter an html element
      //    and will return the data context that was bound when
      //    that html element was rendered!
      if(!before) {
        var orderEL = Blaze.getData(el).order,
          orderAfter = Blaze.getData(after).order;

        // Update order with element after
        collection.update({_id: Blaze.getData(el)._id}, {$set: {order: orderAfter}});
        collection.update({_id: Blaze.getData(after)._id}, {$set: {order: orderEL}});
      }
      else if(!after) {
        var orderEL = Blaze.getData(el).order,
          orderBefore = Blaze.getData(before).order;

        // Update order with element before
        collection.update({_id: Blaze.getData(el)._id}, {$set: {order: orderBefore}});
        collection.update({_id: Blaze.getData(before)._id}, {$set: {order: orderEL}});
      }
      else {
        var orderEL = Blaze.getData(el).order,
          orderBefore = Blaze.getData(before).order,
          orderAfter = Blaze.getData(after).order;

        if (orderEL > orderAfter) {
          // Update order with element before
          collection.update({_id: Blaze.getData(el)._id}, {$set: {order: orderBefore}});
          collection.update({_id: Blaze.getData(before)._id}, {$set: {order: orderEL}});
        }
        else if (orderBefore > orderEL) {
          // Update order with element after
          collection.update({_id: Blaze.getData(el)._id}, {$set: {order: orderAfter}});
          collection.update({_id: Blaze.getData(after)._id}, {$set: {order: orderEL}});
        }
      }
    }
  },

  /* Bulk file save */
  bulkSave: function() {
    var hondaSources = document.getElementsByClassName('window-honda-source');

    for (var i = 0; i < hondaSources.length; i++) {
      if (hondaSources[i].getElementsByClassName('source-content').length !== 0) {
        var text = hondaSources[i].getElementsByClassName('source-content')[0].innerText;
      }
      else {
        var text = hondaSources[i].getElementsByClassName('source-content')[0].innerText;
      }

      saveTextAsFile(text, Narek.Honda.loadedFilesCollection.findOne({order: i + 1}).name + '-HONDA.txt');
    };
  },

  toggleBuildMode: function(state) {
    Session.set('honda-build-mode', state);

    if (state === true) {
      Narek.Honda.initHondaDragAndDrop();
    }
    else {
      Narek.Honda.deInitHondaDragAndDrop();
    }
  },

  toggleCutCopyMode: function(state) {
    Session.set('honda-cut-copy-mode', state);
  }
};

Narek.Honda.History = {
  init: function() {
    var initialState = {};

    this.loadedFilesWindowIdsAndContent = [];
    this.buildFileSentences = [],
    this.activeState = 0;
    this.states = [];

    initialState.loadedFilesWindowIdsAndContent = [];
    initialState.buildFileSentences = [];
    this.states.push(initialState);
  },
  reset: function() {
    this.init();
    Narek.Honda.loadedFilesCollection.remove({});
    Narek.Honda.buildFileSentences.remove({});
  },

  saveState: function(action) {
    var currentState = Narek.Honda.History.getCurrentState();

    currentState.action = action;

    if (this.activeState != this.states.length - 1) {
      this.states = this.states.slice(0, this.activeState + 1);
    }

    this.states.push(currentState);
    this.activeState++;
  },

  getCurrentState: function() {
    var state = {},
      windows = [];

    this.loadedFilesWindowIdsAndContent.forEach(function(fileWindow, index) {
      var element = document.getElementById(fileWindow.id);

      if (element) {
        var sourceFile = Narek.Honda.loadedFilesCollection.findOne({windowId: fileWindow.id}),
          windowObj = {id: sourceFile.windowId, name: sourceFile.name, content: sourceFile.plainText};

        windows.push(windowObj);
      }
    });
    state.loadedFilesWindowIdsAndContent = windows;
    state.buildFileSentences = Narek.Honda.buildFileSentences.find({}, { sort: { order: 1 } }).fetch();

    return state;
  },

  loadState: function(indexState) {
    var state = this.states[indexState];

    Narek.Honda.buildFileSentences.remove({});

    state.loadedFilesWindowIdsAndContent.forEach(function(file, index) {
      var source = Narek.Honda.loadedFilesCollection.findOne({windowId: file.id}),
        id;

      if (source) {
        id = source._id;
      }
      else {
        id = null;
      }

      Narek.Honda.upsertHondaSourcePlainText(file.content, id, {windowId: file.id, name: file.name});
      Narek.Honda.updateLoadedFilesOrder();
    });

    state.buildFileSentences.forEach(function(sentence, index) {
      Narek.Honda.buildFileSentences.insert({text: sentence.text, order: sentence.order});
    });
  },

  undo: function() {
    var previousState = this.activeState - 1;

    // Load previous state.
    if (this.activeState > 0) {
      this.loadState(previousState);
      this.activeState--;
    }
    else {
      console.log('previousState: ' + previousState);
    }
  },
  redo: function() {
    var nextState = this.activeState + 1;

    // Load next state.
    if (this.states[nextState]) {
      this.loadState(nextState);
      this.activeState++;
    }
    else {
      console.log('nextState: ' + nextState);
      console.log(this.states[nextState]);
    }
  }
}

Narek.Honda.History.init();
