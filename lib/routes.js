Router.configure({
  layoutTemplate: 'layoutNarek'
});

Router.onBeforeAction(function(pause) {
  if (! Meteor.userId()) {
    Narek.showMain = new ReactiveVar(false);
    Narek.showHonda = new ReactiveVar(false);
    this.render('login', {to: 'login'});
  } else {
    this.next();
  }
});

Router.map(function() {
  this.route('/', function(){
    if (!Narek.showMain || !Narek.showHonda) {
      Narek.showMain = new ReactiveVar(true);
      Narek.showHonda = new ReactiveVar(false);
    } else {
      Narek.showMain.set(true);
      Narek.showHonda.set(false);
    }
    this.render('main');
  }, {
    name: 'main',
    data: function() {
      var plainTextOreo = Narek.plainTextOreo.get(),
        plainTextKitkat = Narek.plainTextKitkat.get();

      return {
        plainTextOreo: plainTextOreo,
        plainTextKitkat: plainTextKitkat
        //textDisplayKitkat: Narek.Kitkat.buildSentences(plainTextKitkat)
      };
    }
  });

  this.route('/honda', function(){
    if (!Narek.showMain || !Narek.showHonda) {
      Narek.showMain = new ReactiveVar(false);
      Narek.showHonda = new ReactiveVar(true);
    } else {
      Narek.showMain.set(false);
      Narek.showHonda.set(true);
    }
    this.render('honda');
  }, {
    name: 'honda',
    data: function() {
      var plainTextHondaBuild = Narek.Honda.buildFile.get(),
        plainTextOreo = Narek.plainTextOreo.get(),
        plainTextKitkat = Narek.plainTextKitkat.get();

      return {
        plainTextHondaBuild: plainTextHondaBuild,
        plainTextOreo: plainTextOreo,
        plainTextKitkat: plainTextKitkat
      };
    },
    onBeforeAction: function () {
      // console.log('onBeforeAction');
      this.next();
    }
  });

  /*this.route('main', {
    path: '/',
    data: function() {
      var plainTextOreo = Narek.plainTextOreo.get(),
        plainTextKitkat = Narek.plainTextKitkat.get();

      return {
        plainTextOreo: plainTextOreo,
        plainTextKitkat: plainTextKitkat,
        //textDisplayKitkat: Narek.Kitkat.buildSentences(plainTextKitkat)
      };
    }
  });
  this.route('/honda', {
    path: '/honda',
    onBeforeAction: function () {
      // console.log('onBeforeAction');
      this.next();
    }
  });*/
});
