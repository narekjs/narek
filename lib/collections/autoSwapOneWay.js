AutoSwapOneWay = new orion.collection('AutoSwapOneWay', {
  singularName: 'term',
  pluralName: 'terms',
  link: {
    title: 'AutoSwap One-Way'
  },
  tabular: {
    columns: [
      {
        data: "terms",
        title: "Set of one-way swappable (left->right) terms",
        render: function (val, type, doc) {
          return val.join(' <span class="admin-separator">→</span> ');
        }
      }
    ]
  }
});

AutoSwapOneWay.attachSchema(new SimpleSchema({
  terms: {
    type: [String],
    label: "Set of one-way swappable (left->right) terms"
  }/*,
  createdAt: {
    type: Date,
    autoValue: function() {
      if (this.isInsert) {
        return new Date;
      } else if (this.isUpsert) {
        return {$setOnInsert: new Date};
      } else {
        this.unset();
      }
    }
  },
  updatedAt: {
    type: Date,
    autoValue: function() {
      if (this.isUpdate) {
        return new Date();
      }
    },
    denyInsert: true,
    optional: true
  }*/
}));
