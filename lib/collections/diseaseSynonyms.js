DiseaseSynonyms = new orion.collection('DiseaseSynonyms', {
  singularName: 'disease',
  pluralName: 'diseases',
  link: {
    title: 'Disease Synonyms'
  },
  tabular: {
    columns: [
      {
        data: "synonyms",
        title: "Diseases that are synonyms",
        render: function (val, type, doc) {
          return val.join(' <span class="admin-separator">↔</span> ');
        }
      }
    ]
  }
});

DiseaseSynonyms.attachSchema(new SimpleSchema({
  synonyms: {
    type: [String],
    label: "Diseases that are synonyms"
  }
}));
