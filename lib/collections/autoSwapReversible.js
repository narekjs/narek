AutoSwapReversible = new orion.collection('AutoSwapReversible', {
  singularName: 'term',
  pluralName: 'terms',
  link: {
    title: 'AutoSwap Reversible'
  },
  tabular: {
    columns: [
      {
        data: "terms",
        title: "Sets of reversible terms",
        render: function (val, type, doc) {
          /*var wrapped = '';
          for (var i = 0; i < val.length; i++) {
            wrapped += '<span class="admin-array-item">' + val[i] + '</span>';
          }
          return wrapped;*/
          return val.join(' <span class="admin-separator">↔</span> ');
        }
      }
    ]
  }
});

AutoSwapReversible.attachSchema(new SimpleSchema({
  terms: {
    type: [String],
    label: "Array of reversible terms"
  }/*,
  createdAt: {
    type: Date,
    autoValue: function() {
      if (this.isInsert) {
        return new Date;
      } else if (this.isUpsert) {
        return {$setOnInsert: new Date};
      } else {
        this.unset();
      }
    }
  },
  updatedAt: {
    type: Date,
    autoValue: function() {
      if (this.isUpdate) {
        return new Date();
      }
    },
    denyInsert: true,
    optional: true
  }*/
}));
