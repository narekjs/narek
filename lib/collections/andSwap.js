AndSwap = new orion.collection('AndSwap', {
  singularName: 'term',
  pluralName: 'terms',
  link: {
    title: 'And Swap'
  },
  tabular: {
    columns: [
      {
        data: "term",
        title: "Term for use in And Swap"
      }
    ]
  }
});

AndSwap.attachSchema(new SimpleSchema({
  term: {
    type: String,
    label: "Term for use in And Swap"
  }
}));
