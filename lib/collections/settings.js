Settings = new orion.collection('Settings', {
  singularName: 'setting',
  pluralName: 'settings',
  link: {
    title: 'Settings'
  },
  tabular: {
    columns: [
      {
        data: "setting",
        title: "Setting"
      },{
        data: "value",
        title: "Value"
      }
    ]
  }
});

Settings.attachSchema(new SimpleSchema({
  setting: {
    type: String,
    label: "setting"
  },
  value: {
    type: Boolean,
    label: "value"
  }
}));
