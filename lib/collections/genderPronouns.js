GenderPronouns = new orion.collection('GenderPronouns', {
  singularName: 'pronoun',
  pluralName: 'pronouns',
  link: {
    title: 'Gender Pronouns'
  },
  tabular: {
    columns: [
      {
        data: "malePronoun",
        title: "Male pronoun"
      },{
        data: "femalePronoun",
        title: "Female pronoun"
      }
    ]
  }
});

GenderPronouns.attachSchema(new SimpleSchema({
  malePronoun: {
    type: String,
    label: "Male pronoun"
  },
  femalePronoun: {
    type: String,
    label: "Female pronoun"
  }
}));
