Vitals = new orion.collection('Vitals', {
  singularName: 'term',
  pluralName: 'terms',
  link: {
    title: 'Vitals'
  },
  tabular: {
    columns: [
      {
        data: "vitalsAre",
        title: "Vitals are"
      },{
        data: "vitalsShow",
        title: "Vitals show"
      }
    ]
  }
});

Vitals.attachSchema(new SimpleSchema({
  vitalsAre: {
    type: String,
    label: "Vitals are"
  },
  vitalsShow: {
    type: String,
    label: "Vitals show"
  }
}));
