/* Narek object */
Narek = {
  plainTextOreo: new ReactiveVar('', function(oldValue, newValue) {
    if (oldValue !== newValue) {
      return false;
    }

    return true;
  }),
  plainTextKitkat: new ReactiveVar('', function(oldValue, newValue) {
    if (oldValue !== newValue) {
      return false;
    }

    return true;
  }),

  selectedSentenceId: new ReactiveVar(null),
  selectedSentenceContent: new ReactiveVar(''),
  selectedSentenceSource: new ReactiveVar(''),
  originalSentenceContent: new ReactiveVar(''),

  sentencesCollections: [],
  loadedFiles: null,
  loadedFileIndex: 0,
  loadedPairIndex: 0,
  filePairs: [],
  oreoFiles: [],

  createFilePairs: function() {
    var len = Narek.loadedFiles.length;

    for (var i = 0; i < len - 1; i++) {
      if (Narek.loadedFiles[i].name.endsWith("HONDA.txt")) { // Let's see if we have a HONDA
        if (Narek.loadedFiles[i+1].name.endsWith("OREO.txt")) { // If the next file is OREO, we have a pair
          Narek.filePairs.unshift({
            'oreo': Narek.loadedFiles.item(i+1),
            'kitkat': Narek.loadedFiles.item(i)
          });
          i++;
        }
        else if ((i+2 < len) && (Narek.loadedFiles[i+2].name.endsWith("OREO.txt"))) { // OREO can also be after the next, if we have KITKAT also
          Narek.filePairs.unshift({
            'oreo': Narek.loadedFiles.item(i+2),
            'kitkat': Narek.loadedFiles.item(i)
          });
          i += 2;
        }
      }
      else if (Narek.loadedFiles[i].name.endsWith("KITKAT.txt")) { // If no HONDA, KITKAT is fine too
        if (Narek.loadedFiles[i+1].name.endsWith("OREO.txt")) { // If the next file is OREO, we have a pair
          Narek.filePairs.unshift({
            'oreo': Narek.loadedFiles.item(i+1),
            'kitkat': Narek.loadedFiles.item(i)
          });
          i++;
        }
      }
    }

    if (Narek.filePairs.length) {
      Session.set('loadedFolder', true);
      Session.set('dragAndDropActive', false);
      Session.set('sentenceMode', false);
      Session.set('duplicateCheck', true);
      Session.set('differenceCheck', false);
    }
    else {
      console.log('No OREO + KITKAT/HONDA file pairs created!');
    }
  },

  loadFilePair: function(action) {
    var len = Narek.filePairs.length;

    if (!len) {
      console.log('No OREO + KITKAT/HONDA file pairs found!');
      return;
    }
    var file1, file2;

    if (action === 'first') {
      Narek.loadedPairIndex = 0;
    }
    else if (action === 'next') {
      if (Narek.loadedPairIndex < len - 1) {
        Narek.loadedPairIndex++;
      }
      else {
        return;
      }
    }
    else if (action === 'prev') {
      if (Narek.loadedPairIndex) {
        Narek.loadedPairIndex--;
      }
      else {
        return;
      }
    }

    file1 = Narek.filePairs[Narek.loadedPairIndex].oreo;
    file2 = Narek.filePairs[Narek.loadedPairIndex].kitkat;

    if (file1 && file2) {
      Session.set('loadedFileName', file1.name.replace(/\.[a-z0-9]+$/i, ''));
      Session.set('loadedFileName2', file2.name.replace(/\.[a-z0-9]+$/i, ''));
      Session.set('oreoLocked', false);
      Session.set('genderSwapped', false);
      Narek.Sentence.resetSelectedSentenceVariables();
      Narek.History.reset();
      Narek.File.readFile(file1, Narek.File.loadSelectedFileOreo);
      Narek.File.readFile(file2, Narek.File.loadSelectedFileKitkat);
    }
  },

  createOreoFileList: function() {
    var len = Narek.loadedFiles.length;

    for (var i = 0; i < len; i++) {
      if (Narek.loadedFiles[i].name.endsWith('OREO.txt')) { // If the next file is OREO, we have a pair
        Narek.oreoFiles.push(Narek.loadedFiles.item(i));
      }
    }
    if (Narek.oreoFiles.length) {
      console.log('Oreo file list created!');
    }
    else {
      console.log('No OREO files found!');
    }
  },

  savePlainTextContent: function() {
    this.plainTextOreo.set(document.getElementById('window-oreo').innerText);
    this.plainTextKitkat.set(document.getElementById('window-kitkat').innerText);
  }
};
