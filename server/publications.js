Meteor.publish('AutoSwapOneWay', function() {
  return AutoSwapOneWay.find();
});
Meteor.publish('AutoSwapReversible', function() {
  return AutoSwapReversible.find();
});
Meteor.publish('DiseaseSynonyms', function() {
  return DiseaseSynonyms.find();
});
Meteor.publish('GenderPronouns', function() {
  return GenderPronouns.find();
});
Meteor.publish('Vitals', function() {
  return Vitals.find();
});
Meteor.publish('AndSwap', function() {
  return AndSwap.find();
});
Meteor.publish('Settings', function() {
  return Settings.find();
});
