Meteor.startup(function(){
  // Create seed accounts

  if (Meteor.users.find().count() === 0) {
    var now = new Date().getTime();

    var artursId = Accounts.createUser({
      profile: {
        name: "Arturs"
      },
      username: "arturs",
      email: "arturs@codecraft.lv",
      password: "narek1"
    });

    var rudolfsId = Accounts.createUser({
      profile: {
        name: "Rudolfs"
      },
      username: "rudolfs",
      email: "rudolfs@codecraft.lv",
      password: "narek1"
    });

    var amolId = Accounts.createUser({
      profile: {
        name: "Amol"
      },
      username: "amol",
      email: "amalankar@gmail.com",
      password: "narek1"
    })

    var arturs = Meteor.users.findOne(artursId);
    var rudolfs = Meteor.users.findOne(rudolfsId);
    var amol = Meteor.users.findOne(amolId);

    Roles.addUserToRoles(arturs._id, ["admin"]);
    Roles.addUserToRoles(rudolfs._id, ["admin"]);
    Roles.addUserToRoles(amol._id, ["admin"]);
  }

  if (Settings.find().count() === 0){
    Settings.insert({
      setting: "Use alternative autoswap algorithm?",
      value: true
    });
  }
});
